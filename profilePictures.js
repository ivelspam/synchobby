var fs = require("fs-extra");
var glob = require("glob");
var multer = require("multer");
var sharp = require("sharp");
var mimetypes = require("mime-types");
var download = require("download");
var http = require("http");

downloadAFacebookProfilePicture = function(db, AppUserID, facebookUserID, callback){

    console.log("downloadAFacebookProfilePicture");
    var url = `https://graph.facebook.com/${facebookUserID}/picture?width=320`;
    download(url, `public/tempProfilePictures/`, {filename : `${AppUserID}-new`}).then(data => {
        deleteOldPicture(AppUserID, function(){
            console.log(AppUserID)

            editAndSavePicture(db, AppUserID, function(fileInfo){
                callback(fileInfo);
            })
        })
    });
}

deleteOldPicture = function(AppUserID, callback){

    console.log("deleteOldPicture");
    glob(`public/profilePictures/${AppUserID}-*`, function(er, files){
        for(var i = 0; i < files.length; i++){
            fs.unlink(`${files[i]}`, (err)=>{
                if (err) {
                    console.log("failed to delete local image:"+err);
                } else {
                    console.log('successfully deleted local image');
                }
            })
        }
        callback();
    })
}


editAndSavePicture = function(db, AppUser, callback){
    console.log("editAndSavePictures");



    var AppUserID = AppUser.AppUserID
    var ImageVersion = AppUser.ImageVersion
    var NewImageVersion = ImageVersion + 1


    var fileName = `${AppUserID}-${NewImageVersion}`;

    console.log(AppUserID)

    var file = fs.readFileSync(`public/tempProfilePictures/${AppUserID}-new`);

    sharp( file)
        .resize(320, null)
        .max()
        .toFormat("jpeg")
        .toFile(`public/profilePictures/${fileName}.jpg`, (err, fileInfo)=>{
            console.log(`UPDATE appuser SET WHERE AppUserID=${AppUserID}`);

            const query = `UPDATE appuser SET ImageVersion=ImageVersion+1 WHERE AppUserID=${AppUserID}`;

            db.query(query, (err, result)=>{
                if(err){throw err}
                callback(fileInfo);
            })
        });
}



module.exports = {downloadAFacebookProfilePicture, editAndSavePicture, deleteOldPicture};