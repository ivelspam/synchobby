var request = require("request");
var fs = require("fs-extra");
var jwt = require("jsonwebtoken");
var utils = require("./utils");
var http = require("http");
const download = require('download');
var multer = require("multer");
var sharp = require("sharp");
var profilePictures = require("./profilePictures");


var facebookLoginPOST = function(db, req, res){
    console.log("facebookLoginPOST");
    const access_token = fs.readFileSync("facebook.txt", "utf8");

    console.log(req.body);

    const requestURL = `https://graph.facebook.com/debug_token?input_token=${req.body.facebookUserToken}&access_token=${access_token}`;

    console.log(requestURL)

    request.get(requestURL, (err, response, body)=>{
        let facebookTokenInfo = JSON.parse(body);

        console.log(facebookTokenInfo)

        db.query(`SELECT AppUserID FROM FacebookUser WHERE FacebookUserID = ${facebookTokenInfo.data.user_id}`, (err, result)=>{
            if(err){console.log(err)};
            if (err) {db.rollback(function() {throw err;});}

            if (result.length) {

                console.log("found user")

                var payload = {AppUserID : result[0].AppUserID, confirmed : 1, login: "facebook"}
                utils.makeJSONResponse(res, "logged", payload, "Keys/confirmed.key");
            } else {
                request.get(`https://graph.facebook.com/me?access_token=${req.body.facebookUserToken}&fields=first_name, last_name, email, id`, (err, response, bodyFACEBOOK)=>{
                    var facebookRequestBody = JSON.parse(bodyFACEBOOK);
                    db.query(`SELECT AppUserID FROM AppUser WHERE Email = '${facebookRequestBody.email}'`, (err, result)=>{
                        if (err) {db.rollback(function() {throw err;});}
                        if(result.length){
                            var AppUser = result[0];
                            facebookWantToSyncPOST(db, AppUser.AppUserID, facebookRequestBody, (response)=>{
                                console.log("facebookWantToSyncPOST")
                                var payload = {AppUserID : AppUser.AppUserID, confirmed : 1, login: "facebook"};
                                utils.makeJSONResponse(res, "logged",  payload, "Keys/confirmed.key");
                            })
                        }else{
                            facebookCreateANewOne(db, facebookRequestBody, (response)=>{
                                console.log("facebookCreateANewOne")
                                var payload = {AppUserID : response.AppUserID, confirmed : 1, login: "facebook"};
                                utils.makeJSONResponse(res, "logged",  payload, "Keys/confirmed.key");
                            });
                        }
                    })
                });
            }
        })
    });
}


var facebookCreateANewOne = function (db, facebookRequestBody, callback) {
    console.log("facebookCreateANewOne");
    db.beginTransaction((err)=>{
        if(err){db.rollback(()=>{throw err})};

        db.query(`SELECT AUTO_INCREMENT FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'social' AND TABLE_NAME  = 'appuser'`, (err, response)=>{
            db.query(`INSERT INTO AppUser SET Name="${facebookRequestBody.first_name}", Email="${facebookRequestBody.email}", AccountStatus=1, Username='${facebookRequestBody.first_name}${response[0].AUTO_INCREMENT}'`, (err, result)=>{
                var AppUserID = result.insertId;
                if (err) {db.rollback(function() {throw err;});}
                db.query(`INSERT INTO FacebookUser SET  AppUserID="${result.insertId}", FacebookUserID="${facebookRequestBody.id}", Email="${facebookRequestBody.email}", First_name="${facebookRequestBody.first_name}", Last_Name="${facebookRequestBody.last_name}"`,(err, result)=>{
                    if(err){ throw err}
                    db.commit((err)=> {
                        if (err) {db.rollback(function() {throw err;});}
                        // profilePictures.downloadAFacebookProfilePicture(db, AppUserID, facebookRequestBody.id, function(fileInfo){
                            callback({result : "new account", AppUserID : AppUserID});
                        //
                        // })
                    })
                })
            })
        })
    })
}

var facebookWantToSyncPOST = function(db, AppUserID, facebookRequestBody, callback){
    console.log("facebookWantToSyncPOST");
    db.query(`UPDATE AppUser SET Name="${facebookRequestBody.first_name}", AccountStatus=1 WHERE AppUserID=${AppUserID}`, (err, result)=>{
        if (err) {db.rollback(function() {throw err;});}
        db.query(`INSERT INTO FacebookUser SET AppUserID="${AppUserID}", FacebookUserID="${facebookRequestBody.id}", Email="${facebookRequestBody.email}", First_name="${facebookRequestBody.first_name}", Last_Name="${facebookRequestBody.last_name}"`, (err, result)=>{
            if(err){ throw err}
            db.commit((err)=> {
                if (err) {db.rollback(function() {throw err;});}
                callback({result : "synced", AppUserID : AppUserID});
            })
        })
    })
}

// var facebookWantToSyncPOST = function(db, req, res){
//     console.log("facebookWantToSyncPOST");
//
//     if( req.body.facebookUserId === undefined || req.body.facebookUserToken === undefined){
//         console.log("missing parameters");
//         res.json({message: "missing parameters"});
//     }else{
//         const access_token = fs.readFileSync("facebook.txt", "utf8");
//
//         request.get(`https://graph.facebook.com/debug_token?input_token=${req.body.facebookUserToken}&access_token=${access_token}`, (err, response, body)=>{
//             let bodyJSON = JSON.parse(body);
//             console.log(body);
//             console.log('AAAAAAAAAAAAAAAAAAAAAA');
//
//             db.query(`SELECT AppUserID FROM FacebookUser WHERE FacebookUserID = ${bodyJSON.data.user_id}`, (err, result)=>{
//                 if(err){ throw err}
//                 if (result.length) {
//                     console.log("facebook account exists");
//
//                     var payload = {AppUserID : result[0].AppUserID, confirmed : 1, login: "facebook"};
//                     utils.makeJSONResponse(res, "facebook account exists", payload, "Keys/confirmed.key");
//
//                 } else {
//                     request.get(`https://graph.facebook.com/me?access_token=${req.body.facebookUserToken}&fields=first_name, last_name, email, id, age_range`, (err, response, bodyFACEBOOK)=>{
//                         var jsonMeBody = JSON.parse(decodeUnicode(bodyFACEBOOK));
//                         db.query(`SELECT AppUserID FROM AppUser WHERE Email = '${jsonMeBody.email}'`, (err, result)=>{
//                             if (err) {db.rollback(function() {throw err;});}
//                             if(result.length){
//                                 var object = result[0];
//                                 db.beginTransaction((err)=>{
//
//                                     console.log(object);
//                                     db.query(`UPDATE AppUser SET Name="${jsonMeBody.first_name}", AccountConfirmed=1 WHERE AppUserID=${object.AppUserID}`, (err, result)=>{
//
//                                         if (err) {db.rollback(function() {throw err;});}
//                                         db.query(`INSERT INTO FacebookUser SET AppUserID="${object.AppUserID}", FacebookUserID="${jsonMeBody.id}", Email="${jsonMeBody.email}", First_name="${jsonMeBody.first_name}", Last_Name="${jsonMeBody.last_name}", Age_range="${jsonMeBody.age_range.min}"`, (err, result)=>{
//
//                                             if(err){ throw err}
//                                             db.commit((err)=> {
//                                                 if (err) {db.rollback(function() {throw err;});}
//                                                 var payload = {AppUserID : object.AppUserID, confirmed : 1, login: "facebook"};
//                                                 utils.makeJSONResponse(res, "synced", payload, "Keys/confirmed.key")
//                                             })
//                                         })
//                                     })
//                                 })
//                             }else{
//                                 console.log("account do not exists");
//                                 res.json({message: "account do not exists"});
//                             }
//                         })
//                     });
//                 }
//             })
//         });
//     }
//
// }


decodeUnicode = (string)=>{
    var r = /\\u([\d\w]{4})/gi;
    return string.replace(r, function (match, grp) {
        return String.fromCharCode(parseInt(grp, 16)); } );
}

module.exports = {facebookLoginPOST};