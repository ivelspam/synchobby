var cron = require('cron');


var createJob = (cronTime, context)=>{

    new cron.CronJob(
        {
            cronTime: cronTime,

            onTick: function () {
                var date = new Date().getUTCDate();
                // context.db.query(`SELECT * FROM event WHERE EndDateTime=${date}`, ()=>{
                    //
                    // console.log("AAAAAAAAAAAAAAAAA");
                // })
            },
            start : true,
            context: context
        });
}

var updateEvents = (db)=>{
    new cron.CronJob(
        {
            cronTime: "10 * * * * *",
            onTick: function () {
                // console.log("updateEvents");
                var date = new Date().toISOString().slice(0, 19).replace('T', ' ');
                db.beginTransaction(()=>{
                    db.query(`SELECT EventID FROM Event WHERE EndDateTime<"${date}" AND State!=2`, (err, result)=>{
                        if(err){db.rollback(()=>{throw err})};
                        if(!result.length){
                            db.commit((err)=>{
                                if(err){throw err};
                            })
                        }else{
                            var ids = result.map((event)=>{
                                return event.EventID;
                            })
                            db.query(`UPDATE event SET State=2 WHERE EventID IN (${ids})`, (err, result)=>{
                                if(err){db.rollback(()=>{throw err})};
                                db.query(`UPDATE appuser SET EventID=NULL WHERE EventID IN (${ids})`, (err, result)=>{
                                    if(err){db.rollback(()=>{throw err})};
                                    db.commit((err)=>{
                                        if(err){throw err};
                                    })
                                })
                            })
                        }
                    })
                })
            },
            start : true,
        });
}



module.exports = {createJob, updateEvents};