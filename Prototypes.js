Date.prototype.getMySQLUTC = function(){

    var year = this.getUTCFullYear().addLeadingZero(4);
    var month = this.getUTCMonth().addLeadingZero(2);
    var date = this.getUTCDate().addLeadingZero(2);

    var hour = this.getUTCHours().addLeadingZero(2);
    var minute = this.getUTCMinutes().addLeadingZero(2);
    var second = this.getUTCSeconds().addLeadingZero(2);
    var milliseconds = this.getUTCMilliseconds().addLeadingZero(3);


    return `${year}-${month}-${date} ${hour}:${minute}:${second}.${milliseconds}`;

};


Number.prototype.addLeadingZero = function(width)
{
    width -= this.toString().length;
    if ( width > 0 )
    {
        return new Array( width + (/\./.test( this ) ? 2 : 1) ).join( '0' ) + this;
    }
    return this + ""; // always return a string
}

module.exports = {};


