var utils = require("./utils");
var event = require("./Event");


var findAUserByUsernameGET = function(db, req, res){
    console.log("findAUserByUsernameGET");

    db.query(`SELECT Name, AppUserID, ImageVersion FROM AppUser WHERE Username="${req.query.Username}"`, (err, result)=>{
        if(err){throw err;}
        if(result.length){
            utils.makeJSONResponse(res, "userFound", null, null, {user : result[0]});
        }else{
            utils.makeJSONResponse(res, "userNotFound");
        }
    });
}

var getAppUserInfo = function(db, AppUserID, callback) {
    console.log("getAppUserInfo");

    console.log(AppUserID);
    db.query(`SELECT Name, Email, DOB, Username, Distance, MeasureSystem, EventID, ImageVersion, UpdatedTS FROM appuser WHERE AppUserID="${AppUserID}"`, (err, result) => {
        if (err) {throw err;}
        if(result.length){
            console.log(result[0]);
            callback(result[0]);
        }else{
            callback(null);
        }
    });
}


var getUserImageVersion = function(db, req, res){
    console.log("getUserImageVersion");
    let query = `SELECT ImageVersion FROM appuser WHERE AppUserID="${req.query.AppUserID}"`;

    db.query(query, (err, result) => {

        if(result.length){
            utils.makeJSONResponse(res, "foundImage", null, null, {ImageVersion : result[0].ImageVersion});
        }else{
            utils.makeJSONResponse(res, "notFoundImage");
        }
    })
}

var getAppuserUpdates = function(db, AppUserID, UpdatedTS, callback) {
    console.log("getAppuserUpdates");

    let query = `SELECT Name, Email, DOB, Username, Distance, MeasureSystem, EventID, ImageVersion, 
                 UpdatedTS FROM appuser WHERE AppUserID="${AppUserID}" AND UpdatedTS>'${UpdatedTS}'`
    db.query(query, (err, result) => {
        if (err) {throw err;}
        if(result.length){
            callback(result[0]);
        }else{
            callback("noChanges");
        }
    });
}


//params : Username
var updateUsername = function(db, req, res) {
    console.log("updateUserName");

    var body = req.body;
    var tokenData = req.tokenData;

    db.query(`UPDATE IGNORE appuser SET Username='${body.Username}' WHERE AppUserID="${tokenData.AppUserID}"`, (err, result) => {
        console.log(result);
        if (err) {throw err;}
        if(result.changedRows){
            utils.makeJSONResponse(res, "updated");
        }else{
            utils.makeJSONResponse(res, "notUnique");
        }
    });
}


//params : Username
var deleteAccount = function(db, req, res) {
    console.log("deleteAccount");
    var tokenData = req.tokenData;

    db.beginTransaction((err)=> {
        if(err){db.rollback(()=>{throw err})};
        db.query(`UPDATE appuser SET AccountStatus='2' WHERE AppUserID="${tokenData.AppUserID}"`, (err, result) => {
            if(err){db.rollback(()=>{throw err})};
            db.query(`UPDATE friend SET State=5 WHERE AppUserID=${tokenData.AppUserID} OR AppUserFriendID=${tokenData.AppUserID}`, (err, result) => {
                if(err){db.rollback(()=>{throw err})};
                db.commit((err) => {
                    if(err){db.rollback(()=>{throw err})};
                    utils.makeJSONResponse(res, "accountDeleted")
                })
            });
        });
    })
}


//Parameters
//Name
var updateName = function(db, req, res) {
    console.log("updateName");

    var body = req.body;
    var tokenData = req.tokenData;

    db.query(`UPDATE appuser SET Name='${body.Name}' WHERE AppUserID="${tokenData.AppUserID}"`, (err, result) => {
        console.log(result);
        if (err) {throw err;}
        utils.makeJSONResponse(res, "updated");
    });
}

var updateMeasureSystemPOST = function(db, req, res){
    console.log("updateMeasureSystemPOST");
    var tokenData = req.tokenData;
    var body = req.body;

    db.query(`UPDATE AppUser SET MeasureSystem='${body.MeasureSystem}' WHERE AppUserID=${tokenData.AppUserID}`, (err, result)=>{
        if(err){throw err};
        utils.makeJSONResponse(res, "changed", null, null, {MeasureSystem : body.MeasureSystem});

    });
}


//MeasureSystem, Distance
var updateUserDistancePOST = function(db, req, res){
    console.log("updateUserDistancePOST");
    var tokenData = req.tokenData,
        body = req.body;

    console.log(body)
    var query = `UPDATE AppUser SET Distance=${body.Distance}, MeasureSystem='${body.MeasureSystem}' WHERE AppUserID=${tokenData.AppUserID}`
    console.log(query)
    db.query(query, (err, result)=>{
        if(err){throw err};

        event.eventsByDistanceWithUserAppID(db, tokenData.AppUserID, body.MeasureSystem, body.Distance, body.Latitude, body.Longitude, (result)=>{
            if(result.length){
                utils.makeJSONResponse(res, "found", null, null, {Activities : result});
            }else{
                utils.makeJSONResponse(res, "not found");
            }
        });
    });
}

var updateDistanceAndMeasurement = function(db, req, res){
    console.log("updateUserDistancePOST");
    var tokenData = req.tokenData,
        body = req.body;

    db.query(`UPDATE AppUser SET Distance=${body.Distance}, MeasureSystem='${body.MeasureSystem}' WHERE AppUserID=${tokenData.AppUserID}`, (err, result)=>{
        if(err){throw err};
        if(result.affectedRows){
            utils.makeJSONResponse(res, "updated");
        }else{
            utils.makeJSONResponse(res, "notUpdated");
        }
    });
}


checkIfUserExists = function(db, req, res){
    console.log("checkIfUserExists");
    console.log(req.tokenData);

    db.query(`SELECT * FROM appuser WHERE AppUserID=${req.tokenData.AppUserID}`, (err, result)=> {
        if(err){throw err};
        if(result.length){

            console.log('userFound')
            utils.makeJSONResponse(res, "userFound")
        }else{
            console.log('userNotFound')
            utils.makeJSONResponse(res, "userNotFound")
        }
    })
}



module.exports = {
            getAppUserInfo,
            updateUsername,
            updateName,
            findAUserByUsernameGET,
            updateDistanceAndMeasurement,
            getAppuserUpdates,
            getUserImageVersion,
            checkIfUserExists,
            deleteAccount

};


