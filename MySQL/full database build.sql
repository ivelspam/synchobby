 DROP SCHEMA IF EXISTS  social;
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema social
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema social
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `social` DEFAULT CHARACTER SET utf8 ;
USE `social` ;

-- -----------------------------------------------------
-- Table `social`.`AppUser`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`AppUser` (
  `AccountStatus` TINYINT NOT NULL DEFAULT 0,
  `AppUserID` INT NOT NULL AUTO_INCREMENT,
  `CreatedTS` TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `Distance` INT NOT NULL DEFAULT 50,
  `DOB` DATETIME(5) NULL,
  `Email` VARCHAR(255) NOT NULL,
  `EventID` INT NULL DEFAULT NULL,
  `ImageVersion` INT NOT NULL DEFAULT 0,
  `Name` VARCHAR(45) NOT NULL,
  `MeasureSystem` VARCHAR(10) NOT NULL DEFAULT 'mi',
  `UpdatedTS` TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  `Username` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`AppUserID`),
  UNIQUE INDEX `Email_UNIQUE` (`Email` ASC),
  UNIQUE INDEX `Username_UNIQUE` (`Username` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `social`.`Hobby`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`Hobby` (
  `HobbyID` INT NOT NULL,
  `Name` VARCHAR(45) NOT NULL,
  `Modification` VARCHAR(45) NOT NULL,
  `ParentID` INT NULL,
  `Type` VARCHAR(45) NOT NULL,
  `Version` INT NOT NULL,
  PRIMARY KEY (`HobbyID`),
  UNIQUE INDEX `Name_UNIQUE` (`Name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `social`.`Event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`Event` (
  `EventID` INT NOT NULL AUTO_INCREMENT,
  `Address` VARCHAR(500) NOT NULL,
  `AppUserID` INT NOT NULL,
  `CreatedTS` TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  `EndDatetime` DATETIME(5) NOT NULL,
  `Description` VARCHAR(400) NULL,
  `HobbyID` INT NOT NULL,
  `Latitude` DECIMAL(10,8) NOT NULL,
  `Longitude` DECIMAL(11,8) NOT NULL,
  `Name` VARCHAR(45) NULL,
  `PlaceName` VARCHAR(45) NOT NULL DEFAULT '',
  `QuantityOfPeople` INT NOT NULL DEFAULT 1,
  `Reach` TINYINT NOT NULL DEFAULT 0,
  `StartDatetime` DATETIME(5) NOT NULL,
  `State` TINYINT NOT NULL DEFAULT 0,
  `UpdatedTS` TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  `Hidden` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`EventID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `social`.`AppUserHobby`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`AppUserHobby` (
  `AppUserID` INT NOT NULL,
  `HobbyID` INT NOT NULL,
  PRIMARY KEY (`AppUserID`, `HobbyID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `social`.`FacebookUser`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`FacebookUser` (
  `AppUserID` INT NOT NULL,
  `Email` VARCHAR(321) NOT NULL,
  `FacebookUserID` VARCHAR(45) NOT NULL,
  `First_Name` VARCHAR(45) NOT NULL,
  `Last_Name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`AppUserID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `social`.`EmailUser`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`EmailUser` (
  `AppUserID` INT NOT NULL,
  `Confirmed` TINYINT NOT NULL DEFAULT 0,
  `CreatedTS` TIMESTAMP(3) NULL DEFAULT CURRENT_TIMESTAMP(3),
  `Password` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`AppUserID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `social`.`EmailOneTimeToken`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`EmailOneTimeToken` (
  `CreatedTS` TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `EmailOneTimeTokenID` INT NOT NULL AUTO_INCREMENT,
  `ExpirationTime` INT NOT NULL,
  `Token` VARCHAR(200) NOT NULL,
  `UpdatedTS` TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`EmailOneTimeTokenID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `social`.`Friend`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`Friend` (
  `AppUserID` INT NOT NULL,
  `AppUserFriendID` INT NOT NULL,
  `CreatedTS` TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `State` TINYINT NOT NULL DEFAULT 0,
  `UpdatedTS` TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`AppUserID`, `AppUserFriendID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `social`.`hobbyclosure`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`hobbyclosure` (
  `Ancestor` INT NOT NULL,
  `Descendant` INT NOT NULL,
  `Depth` INT NOT NULL,
  `HobbyClosureID` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`HobbyClosureID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `social`.`FCMToken`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`FCMToken` (
  `AppUserID` INT NOT NULL,
  `DeviceID` VARCHAR(100) NOT NULL,
  `DeviceType` VARCHAR(45) NOT NULL,
  `FCMTokenID` VARCHAR(255) NOT NULL,
  `Timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`DeviceID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `social`.`Chat`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`Chat` (
  `ChatID` INT NOT NULL AUTO_INCREMENT,
  `CreatedTS` TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `Owner` INT NOT NULL,
  `Message` VARCHAR(255) NOT NULL,
  `Receiver` INT NOT NULL,
  `SentDatetime` DATETIME(3) NOT NULL DEFAULT NOW(3),
  `State` TINYINT(1) NOT NULL DEFAULT 1,
  `UpdatedTS` TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  `UUID` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`ChatID`),
  UNIQUE INDEX `UUID_UNIQUE` (`UUID` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `social`.`test`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`test` (
  `Datetime` DATETIME(3) NULL)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `social`.`EventComment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`EventComment` (
  `AppUserID` INT NOT NULL,
  `Comment` VARCHAR(500) NOT NULL,
  `CreatedTS` TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `EventCommentID` INT NOT NULL AUTO_INCREMENT,
  `EventID` INT NOT NULL,
  `UUID` VARCHAR(60) NULL,
  PRIMARY KEY (`EventCommentID`))
ENGINE = InnoDB;

USE `social` ;

-- -----------------------------------------------------
-- Placeholder table for view `social`.`AppUserEmailUser`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`AppUserEmailUser` (`AccountStatus` INT, `AppUserID` INT, `Confirmed` INT, `Email` INT, `Name` INT, `Password` INT, `Username` INT);

-- -----------------------------------------------------
-- Placeholder table for view `social`.`EventFullInfo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`EventFullInfo` (`Address` INT, `AppUserID` INT, `AppUserName` INT, `CreatedTS` INT, `EndDatetime` INT, `Description` INT, `EventID` INT, `Hidden` INT, `HobbyID` INT, `HobbyName` INT, `Latitude` INT, `Longitude` INT, `Name` INT, `PlaceName` INT, `QuantityOfPeople` INT, `Reach` INT, `StartDatetime` INT, `State` INT, `UpdatedTS` INT);

-- -----------------------------------------------------
-- Placeholder table for view `social`.`FriendsInEvent`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`FriendsInEvent` (`AppUserFriendID` INT, `AppUserID` INT, `EventID` INT);

-- -----------------------------------------------------
-- Placeholder table for view `social`.`FriendInfo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`FriendInfo` (`AppUserID` INT, `EventID` INT, `ImageVersion` INT, `Name` INT, `State` INT, `UpdatedTS` INT, `ViewAppUserID` INT);

-- -----------------------------------------------------
-- Placeholder table for view `social`.`EventCommentInfo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `social`.`EventCommentInfo` (`AppUserID` INT, `Comment` INT, `CreatedTS` INT, `EventCommentID` INT, `EventID` INT, `ImageVersion` INT, `Name` INT, `UUID` INT);

-- -----------------------------------------------------
-- View `social`.`AppUserEmailUser`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `social`.`AppUserEmailUser`;
USE `social`;
CREATE  OR REPLACE VIEW `AppUserEmailUser` AS
    SELECT 
        AccountStatus,
		t1.AppUserID as AppUserID,
        Confirmed,
        Email,
        `Name`,
        `Password`,
        Username
    FROM
        AppUser t1
           LEFT JOIN
        EmailUser t2 ON t2.AppUserID = t1.AppUserID;

-- -----------------------------------------------------
-- View `social`.`EventFullInfo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `social`.`EventFullInfo`;
USE `social`;
CREATE  OR REPLACE VIEW `EventFullInfo` AS
	SELECT 
			 event.Address, 
             event.AppUserID,
			(Select Name From appuser WHERE appuser.AppUserID = event.AppUserID) as AppUserName,
             event.CreatedTS,
             event.EndDatetime,
             event.Description,
             event.EventID,
			 event.Hidden,
             event.HobbyID,
             (Select Name From hobby WHERE hobby.HobbyID = event.HobbyID) as HobbyName, 
             event.Latitude,
             event.Longitude,
             event.`Name`,
             event.PlaceName,
             event.QuantityOfPeople,
             event.Reach,
             event.StartDatetime,
             event.State,
             event.UpdatedTS
    FROM event;

-- -----------------------------------------------------
-- View `social`.`FriendsInEvent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `social`.`FriendsInEvent`;
USE `social`;
/*
CREATE  OR REPLACE VIEW `FriendsOnActivity` AS
SELECT friend.AppUserID, friend.AppUserFriendID FROM appuserinactivityrunning
LEFT JOIN friend ON friend.AppUserFriendID = appuserinactivityrunning.AppUserID;
*/

CREATE VIEW `FriendsInEvent` AS
SELECT friend.AppUserFriendID, AppUser.AppUserID, AppUser.EventID  FROM AppUser
LEFT JOIN friend ON friend.AppUserID = AppUser.AppUserID;

-- -----------------------------------------------------
-- View `social`.`FriendInfo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `social`.`FriendInfo`;
USE `social`;
CREATE  OR REPLACE VIEW `FriendInfo` AS
SELECT 
friend.AppUserFriendID AS AppUserID, 
EventID, 
ImageVersion, 
AppUser.Name, 
friend.State, 
IF(friend.UpdatedTS > AppUser.UpdatedTS, friend.UpdatedTS, AppUser.UpdatedTS ) as UpdatedTS,
friend.AppUserID as ViewAppUserID 

From friend
LEFT JOIN AppUser ON friend.AppUserFriendID = AppUser.AppUserID;

-- -----------------------------------------------------
-- View `social`.`EventCommentInfo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `social`.`EventCommentInfo`;
USE `social`;
CREATE  OR REPLACE VIEW EventCommentInfo AS

SELECT 
eventcomment.AppUserID, 
eventcomment.`Comment`, 
eventcomment.CreatedTS, 
eventcomment.EventCommentID, 
eventcomment.EventID, 
ImageVersion, 
Name, 
eventcomment.UUID
FROM eventcomment 
Left JOIN appuser ON appuser.AppUserID = eventcomment.AppUserID;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;



INSERT INTO `social`.`hobby`
(`HobbyID`,
`Name`,
`Type`,
`Version`,
`ParentID`,
`Modification`
)
VALUES
('1', 'video game', 'parent', '1', NULL, 'add'),
('2', 'Street Fighter 5', 'child', '1', '1', 'add'),
('3', 'FIFA', 'child', '1', '1', 'add'),
('4', 'trading card game', 'parent', '1', NULL, 'add'),
('5', 'magic: the gathering', 'parent', '1', '4', 'add'),
('6', 'magic: the gathering - modern', 'child', '1', '5', 'add'),
('7', 'magic: the gathering - edh', 'child', '1', '5', 'add'),
('8', 'magic: the gathering - standard', 'child', '1', '5', 'add'),
('9', 'card game', 'parent', '1', NULL, 'add'),
('10', 'poker', 'child', '1', '9', 'add'),
('11', '21', 'child', '1', '9', 'add'),
('12', 'nertz', 'child', '1', '9', 'add'),
('13', 'dancing', 'parent', '1', NULL, 'add'),
('14', 'hip-hop', 'child', '1', '13', 'add'),
('15', 'board game', 'parent', '1', NULL, 'add'),
('16', 'moonopoly', 'child', '1', '15', 'add'),
('17', 'catan', 'child', '1', '15', 'add'),
('18', 'hiking', 'parent', '1', NULL, 'add'),
('19', 'Mountain Trails', 'child', '1', '18', 'add'),
('20', 'sports', 'parent', '1', NULL, 'add'),
('21', 'soccer', 'child', '1', '20', 'add'),
('22', 'RPG', 'parent', '1', NULL, 'add'),
('23', 'live action role-player (LARP)', 'child', '1', '22', 'add'),
('24', 'tabletop role-player', 'child', '1', '22', 'add'),
('25', 'movie', 'parent', '1', NULL, 'add'),
('26', 'movie night', 'child', '1', '25', 'add'),
('27', 'go to the movies', 'child', '1', '25', 'add');



INSERT INTO `social`.`hobbyclosure`
(`Ancestor`,
`Descendant`,
`Depth`,
`HobbyClosureID`)
VALUES 
(1, 1, 0, 1   ),
(1, 2, 1, 2   ),
(1, 3, 1, 5   ),
(2, 2, 0, 3   ),
(3, 3, 0, 6   ),
(4, 4, 0, 8   ),
(4, 5, 1, 9   ),
(4, 6, 2, 12  ),
(4, 7, 2, 15  ),
(4, 8, 2, 18  ),
(5, 5, 0, 10  ),
(5, 6, 1, 13  ),
(5, 7, 1, 16  ),
(5, 8, 1, 19  ),
(6, 6, 0, 14  ),
(7, 7, 0, 17  ),
(8, 8, 0, 20  ),
(9, 9, 0, 21  ),
(9, 10, 1, 22 ),
(9, 11, 1, 25 ),
(9, 12, 1, 28 ),
(10, 10, 0, 23),
(11, 11, 0, 26),
(12, 12, 0, 29),
(13, 13, 0, 31),
(13, 14, 1, 32),
(14, 14, 0, 33),
(15, 15, 0, 35),
(15, 16, 1, 36),
(15, 17, 1, 39),
(16, 16, 0, 37),
(17, 17, 0, 40),
(18, 18, 0, 42),
(18, 19, 1, 43),
(19, 19, 0, 44),
(20, 20, 0, 46),
(20, 21, 1, 47),
(21, 21, 0, 48),
(22, 22, 0, 50),
(22, 23, 1, 51),
(22, 24, 1, 54),
(23, 23, 0, 52),
(24, 24, 0, 55),
(25, 25, 0, 57),
(25, 26, 1, 58),
(25, 27, 1, 61),
(26, 26, 0, 59),
(27, 27, 0, 62);

set @@global.time_zone = '+00:00';
set @@session.time_zone = '+00:00';

SET GLOBAL time_zone = '+00:00';

SELECT @@global.time_zone;
SELECT @@session.time_zone;

