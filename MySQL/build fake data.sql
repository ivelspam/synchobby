-- users
START TRANSACTION;
set @temp = (SELECT `AUTO_INCREMENT` FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'social' AND   TABLE_NAME   = 'appuser');

select @temp;

INSERT INTO appuser (Name, Email, Username, EventID, ImageVersion, AccountStatus) VALUES ('Nicholas', 'ivelacc@gmail.com', CONCAT("Nicholas", @temp), 1, 1, 1);
INSERT INTO appuserhobby (HobbyID, AppUserID) VALUES (6, 1);
INSERT INTO appuserhobby (HobbyID, AppUserID) VALUES (11, 1);
INSERT INTO `social`.`emailuser` (`AppUserID`, `Confirmed`, `Password`) 
VALUES ('1', '1', '$2a$10$kc6TNzRbcGe8U.IQM4igPeg9QK2a4hDyOu0L0laFxcWYbmNach02i');

COMMIT;


START TRANSACTION;
set @temp = (SELECT `AUTO_INCREMENT`
			FROM  INFORMATION_SCHEMA.TABLES
			WHERE TABLE_SCHEMA = 'social'
			AND   TABLE_NAME   = 'appuser');

select @temp;

INSERT INTO appuser (Name, Email, Username, EventID, ImageVersion, AccountStatus) VALUES ('Jose', 'junior_yxpksug_araujo@tfbnw.net', CONCAT("Jose", @temp), 2, 1, 1);
INSERT INTO `social`.`emailuser` (`AppUserID`, `Confirmed`, `Password`) 
VALUES ('2', '1', '$2a$10$kc6TNzRbcGe8U.IQM4igPeg9QK2a4hDyOu0L0laFxcWYbmNach02i');

COMMIT;

INSERT INTO friend (AppUserID, AppUserFriendID, State) VALUES (1, 2, 2);
INSERT INTO friend (AppUserID, AppUserFriendID, State) VALUES (2, 1, 2);



START TRANSACTION;
set @temp = (SELECT `AUTO_INCREMENT`
			FROM  INFORMATION_SCHEMA.TABLES
			WHERE TABLE_SCHEMA = 'social'
			AND   TABLE_NAME   = 'appuser');

select @temp;
INSERT INTO appuser (Name, Email, Username, EventID, ImageVersion, AccountStatus) VALUES ('Vicente', 'kolob@gmail.com', CONCAT("Vicente", @temp), 2, 1, 1);

COMMIT;

INSERT INTO friend (AppUserID, AppUserFriendID, State) VALUES (1, 3, 2);
INSERT INTO friend (AppUserID, AppUserFriendID, State) VALUES (3, 1, 2);


/*

START TRANSACTION;
set @temp = (SELECT `AUTO_INCREMENT`
			FROM  INFORMATION_SCHEMA.TABLES
			WHERE TABLE_SCHEMA = 'social'
			AND   TABLE_NAME   = 'appuser');

select @temp;
INSERT INTO appuser (Name, Email, Username, ImagePath, EventID) VALUES ('Phillip', 'phillip@gmail.com', CONCAT("Phillip", @temp), CONCAT(@temp, -1510692177270, ".jpg"), null);

COMMIT;

-- relationships
INSERT INTO friend (AppUserID, AppUserFriendID, State) VALUES (1, 2, 0);
INSERT INTO friend (AppUserID, AppUserFriendID, State) VALUES (2, 1, 0);
INSERT INTO friend (AppUserID, AppUserFriendID, State) VALUES (1, 3, 1);
INSERT INTO friend (AppUserID, AppUserFriendID, State) VALUES (3, 1, 1);

-- events
*/

INSERT INTO `social`.`event`(`EventID`, `AppUserID`, `Name`, `HobbyID`, `Address`, `Latitude`, `Longitude`, `StartDatetime`, `EndDatetime`, `Reach`, `Description`, `PlaceName`)
	VALUES ('1', '1', 'Hello World', '6', '1143 Center St, Orem, UT 84057, USA', '40.29699360', '-111.72291160', current_timestamp(), DATE_ADD(current_timestamp(), INTERVAL 4 HOUR), 0, 'This is the first event.', 'casa ');


INSERT INTO `social`.`event`(`EventID`, `AppUserID`, `Name`, `HobbyID`, `Address`, `Latitude`, `Longitude`, `StartDatetime`, `EndDatetime`, `Reach`, `Description`, `PlaceName`)
	VALUES ('2', '2', 'Hello World 2', '7', '1143 Center St, Orem, UT 84057, USA', '40.29699360', '-111.72291160', current_timestamp(), DATE_ADD(current_timestamp(), INTERVAL 4 HOUR), 0, 'This is the first event.', 'casa 2');


-- messages

INSERT INTO chat (`Owner`, `UUID`, `Receiver`, `State`, `Message`) VALUES (1, 'c71c018e-10b5-11e8-b642-0ed5f89f718b', 2, 4, 'Primeira Message Primeira Message Primeira Message Primeira Message Primeira Message Primeira Message Primeira Message Primeira Message ');
INSERT INTO chat (`Owner`, `UUID`, `Receiver`, `State`, `Message`) VALUES (2, 'c71c044a-10b5-11e8-b642-0ed5f89f718b', 1, 4, 'Segunda Message Segunda Message Segunda Message Segunda Message Segunda Message Segunda Message Segunda Message Segunda Message Segunda ');
INSERT INTO chat (`Owner`, `UUID`, `Receiver`, `State`, `Message`) VALUES (1, 'c71c05a8-10b5-11e8-b642-0ed5f89f718b', 2, 4, 'Terceira Message');
INSERT INTO chat (`Owner`, `UUID`, `Receiver`, `State`, `Message`) VALUES (1, 'c71c07ba-10b5-11e8-b642-0ed5f89f718b', 2, 4, 'Quarta Message');
INSERT INTO chat (`Owner`, `UUID`, `Receiver`, `State`, `Message`) VALUES (2, 'c71c0fee-10b5-11e8-b642-0ed5f89f718b', 1, 4, 'Quinta Message');
INSERT INTO chat (`Owner`, `UUID`, `Receiver`, `State`, `Message`) VALUES (2, 'c71c1138-10b5-11e8-b642-0ed5f89f718b', 1, 4, 'Sexta Message');
INSERT INTO chat (`Owner`, `UUID`, `Receiver`, `State`, `Message`) VALUES (1, 'c71c1264-10b5-11e8-b642-0ed5f89f718b', 2, 3, 'Setima Message');
INSERT INTO chat (`Owner`, `UUID`, `Receiver`, `State`, `Message`) VALUES (2, 'c71c1390-10b5-11e8-b642-0ed5f89f718b', 1, 3, 'oitava Message');
INSERT INTO chat (`Owner`, `UUID`, `Receiver`, `State`, `Message`) VALUES (1, 'c71c16b0-10b5-11e8-b642-0ed5f89f718b', 2, 3, 'nona Message');
INSERT INTO chat (`Owner`, `UUID`, `Receiver`, `State`, `Message`) VALUES (2, 'c71c1822-10b5-11e8-b642-0ed5f89f718b', 1, 3, 'decima Message');
INSERT INTO chat (`Owner`, `UUID`, `Receiver`, `State`, `Message`) VALUES (2, 'c71c1823-10b5-11e8-b642-0ed5f89f718b', 1, 3, '11 Message');


INSERT INTO `social`.`eventcomment` (`UUID`, `EventID`, `Comment`, `AppUserID`) VALUES("fsdfsdfsdf1", 1, "primeira primeira primeira primeira primeira primeira primeira primeira primeira primeira primeira primeira primeira primeira ", 2);
INSERT INTO `social`.`eventcomment` (`UUID`, `EventID`, `Comment`, `AppUserID`) VALUES("fsdfsdfsdf2", 1, "segunda", 2);
INSERT INTO `social`.`eventcomment` (`UUID`, `EventID`, `Comment`, `AppUserID`) VALUES("fsdfsdfsdf3", 1, "terceira terceira terceira terceira terceira terceira terceira terceira terceira terceira terceira terceira terceira terceira ", 2);
INSERT INTO `social`.`eventcomment` (`UUID`, `EventID`, `Comment`, `AppUserID`) VALUES("fsdfsdfsdf4", 1, "quinta", 2);
INSERT INTO `social`.`eventcomment` (`UUID`, `EventID`, `Comment`, `AppUserID`) VALUES("fsdfsdfsdf5", 1, "sexta", 2);
INSERT INTO `social`.`eventcomment` (`UUID`, `EventID`, `Comment`, `AppUserID`) VALUES("fsdfsdfsdf6", 1, "setima", 2);
INSERT INTO `social`.`eventcomment` (`UUID`, `EventID`, `Comment`, `AppUserID`) VALUES("fsdfsdfsdf7", 1, "oitava", 2);
