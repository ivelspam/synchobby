INSERT INTO Hobby (`Name`, ParentID) VALUES (  "Video Games", null);
INSERT INTO Hobby (`Name`, ParentID) VALUES (  "Street Fighter 5", (SELECT HobbyID FROM ((SELECT * FROM hobby) as table1) WHERE `Name` = "Video Games"));
INSERT INTO Hobby (`Name`, ParentID) VALUES (  "Super Smash Brothers Melee", (SELECT HobbyID FROM ((SELECT * FROM hobby) as table1) WHERE `Name` = "Video Games"));

INSERT INTO Hobby (`Name`, ParentID) VALUES (  "Cards Games", null);

INSERT INTO Hobby (`Name`, ParentID) VALUES (  "Collectible Card Games", (SELECT HobbyID FROM ((SELECT * FROM hobby) as table1) WHERE `Name` = "Cards Games"));
INSERT INTO Hobby (`Name`, ParentID) VALUES (  "Magic: The Gathering", (SELECT HobbyID FROM ((SELECT * FROM hobby) as table1) WHERE `Name` = "Collectible Card Games"));
INSERT INTO Hobby (`Name`, ParentID) VALUES (  "MTG: Standard", (SELECT HobbyID FROM ((SELECT * FROM hobby) as table1) WHERE `Name` = "Magic: The Gathering"));
INSERT INTO Hobby (`Name`, ParentID) VALUES (  "MTG: Modern", (SELECT HobbyID FROM ((SELECT * FROM hobby) as table1) WHERE `Name` = "Magic: The Gathering"));
INSERT INTO Hobby (`Name`, ParentID) VALUES (  "MTG: Commander", (SELECT HobbyID FROM ((SELECT * FROM hobby) as table1) WHERE `Name` = "Magic: The Gathering"));

INSERT INTO Hobby (`Name`, ParentID) VALUES (  "Family Games", (SELECT HobbyID FROM ((SELECT * FROM hobby) as table1) WHERE `Name` = "Cards Games"));
INSERT INTO Hobby (`Name`, ParentID) VALUES (  "Nertz", (SELECT HobbyID FROM ((SELECT * FROM hobby) as table1) WHERE `Name` = "Family Games"));
INSERT INTO Hobby (`Name`, ParentID) VALUES (  "UNO", (SELECT HobbyID FROM ((SELECT * FROM hobby) as table1) WHERE `Name` = "Family Games"));


