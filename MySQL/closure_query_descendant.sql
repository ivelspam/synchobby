SELECT 
    c.*
FROM
    hobby c
        JOIN
    hobby_closure cc ON (c.HobbyID = cc.Descendant)
WHERE
    cc.ancestor = 1