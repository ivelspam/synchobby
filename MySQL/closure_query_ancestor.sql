SELECT 
    c.*
FROM
    hobby c
        JOIN
    hobby_closure cc ON (c.HobbyID = cc.Ancestor)
WHERE
    cc.Descendant = 1