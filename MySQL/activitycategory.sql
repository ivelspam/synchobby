select group_concat(n.Name order by n.HobbyID separator ' -> ') as path
from hobbyclosure d
join hobbyclosure a on (a.descendant = d.descendant)
join hobby n on (n.HobbyID = a.ancestor)
where d.ancestor = 2 and d.descendant != d.ancestor
group by d.descendant;

SELECT * FROM hobby table1
JOIN hobbyclosure table2
on (table1.HobbyID = table2.Ancestor)
join appuserhobby as table3
ON 	(table3.HobbyID = table2.Descendant OR table3.HobbyID = table2.Ancestor)
;

SELECT table4.HobbyID, table4.Name, table4.Type, table2.Descendant, table2.Depth FROM hobby table1
JOIN hobbyclosure table2
on (table1.HobbyID = table2.Descendant)
JOIN appuserhobby table3
on (table1.HobbyID = table3.HobbyID AND table3.AppUserID = 1)
LEFT JOIN hobby table4
on (table2.Ancestor = table4.HobbyID);


INSERT INTO hobbyclosure (ancestor, descendant, depth)
SELECT ancestor, 8, (Depth + 1) FROM hobbyclosure
WHERE descendant = 5 
UNION ALL SELECT 8, 8, 0;
-- Search depth

SELECT * FROM hobby c
JOIN hobbyclosure cc
on (c.HobbyID = cc.Descendant)
WHERE cc.Ancestor = 34;


-- Query Ancestor 

SELECT DISTINCT c.* FROM hobby c
JOIN
    hobbyclosure cc ON (c.HobbyID = cc.Ancestor)
WHERE
    cc.Descendant = 34;

-- query Ancestor of user hobbies

SELECT DISTINCT c.* FROM hobby c
JOIN
    hobbyclosure cc ON (c.HobbyID = cc.Ancestor)
WHERE  (SELECT 1 from appuserhobby WHERE appuserhobby.AppUserID = 1 and cc.Descendant = appuserhobby.HobbyID);
    

-- Query Descendant
SELECT c.* FROM hobby c
JOIN
    hobbyclosure cc ON (c.HobbyID = cc.Descendant)
WHERE
    cc.ancestor = 2;
    
-- Query Descendant
SELECT c.* FROM hobby c
JOIN
    hobbyclosure cc ON (c.HobbyID = cc.Descendant)
WHERE
	cc.ancestor = 2 AND depth = 1;

select *
from hobby
where
      HobbyID not in (
           select descendant
           from hobbyclosure
           where depth > 0
           );
  
   SELECT * FROM hobby
   WHERE NOT EXISTS ( SELECT 1 FROM hobbyclosure WHERE Descendant = HobbyID AND depth > 0 LIMIT 1);
      
      
      SELECT * FROM 
      (  SELECT DISTINCT c.* FROM hobby c
JOIN
    hobbyclosure cc ON (c.HobbyID = cc.Ancestor)
WHERE  (SELECT 1 from appuserhobby WHERE appuserhobby.AppUserID = 1 and cc.Descendant = appuserhobby.HobbyID)) newTable
      
   WHERE NOT EXISTS ( SELECT 1 FROM hobbyclosure cc2 WHERE cc2.Descendant = HobbyID AND depth > 0 LIMIT 1);
  
  
  SELECT DISTINCT c.* FROM hobby c
JOIN
    hobbyclosure cc ON (c.HobbyID = cc.Ancestor)
WHERE  (SELECT 1 from appuserhobby WHERE appuserhobby.AppUserID = 1 and cc.Descendant = appuserhobby.HobbyID);



-- Delete Without childs
SELECT * FROM hobby c
JOIN
    hobbyclosure cc ON (c.HobbyID = cc.Ancestor)

WHERE Type="Hobby"
  