TRUNCATE event;
TRUNCATE appuserinevent;


CREATE VIEW `EventFullInfo` AS
	SELECT 
		t1.EventID
    FROM
		event as t1
			LEFT JOIN appuserinevent AS t2 ON t1.EventID = t1.EventID
	GROUP BY t2.EventID;