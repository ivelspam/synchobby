var jwt = require("jsonwebtoken");
var fs = require("fs-extra");

ensureToken = function(req, res, next){
    console.log("ensureToken");
    const bearerHeader = req.headers["authorization"];


    console.log(req.headers);
   if(typeof  bearerHeader !== 'undefined'){
        const bearer = bearerHeader.split(" ");
        const bearerToken = bearer[1];

        req.token = bearerToken;
        next();
    }else{
        res.sendStatus(403)
    }
}

authTokenPrivate = function(req, res, next){
    console.log("authTokenPrivate");
    jwt.verify(req.token, fs.readFileSync("Keys/confirmed.key", "utf8"), function(err, data) {
        if(err){
            console.log("403")
            res.sendStatus(403)
        }else{
            req.tokenData = data;
            next();
        }
    })
}

authTokenNotConfirmed = function(req, res, next){
    console.log("authTokenNotConfirmed");
    jwt.verify(req.token, fs.readFileSync("Keys/notConfirmed.key", "utf8"), function(err, data) {
        console.log(data);
        if(err){
            console.log("403")
            res.sendStatus(403)
        }else{
            req.tokenData = data;
            next();
        }
    })
}

checkIfUserExists = function(db, req, res, next){
    return function(req, res, next){
        console.log("checkIfUserExists")

        db.query(`SELECT * FROM appuser WHERE AppUserID=${req.tokenData.AppUserID}`, (err, result)=> {
            if(err){throw err};
            if(result.length){
                req.AppUser = result[0];
                next();
            }else{
                res.sendStatus(403);
            }
        })
    }
}

module.exports = {authTokenPrivate, ensureToken, checkIfUserExists, authTokenNotConfirmed};