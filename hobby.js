var utils = require("./utils");
var fs = require("fs-extra");


createClosureTable = function (db) {
    console.log("createClosureTable");
    db.query(`TRUNCATE TABLE hobby`, (err, response)=>{
        if(err){throw err};
        db.query(`TRUNCATE TABLE hobbyclosure;`, (err, response)=>{
            if(err){throw err};
            var hobbies = JSON.parse(fs.readFileSync("./JSON/hobby"));

            closureTableRecursive(db, hobbies);
        })
    })
}

createClosureFromAdjacent = function (db) {
    console.log("createClosureFromAdjacent");



    db.query(`TRUNCATE TABLE hobby`, (err, response)=>{
        if(err){throw err};
        db.query(`TRUNCATE TABLE hobbyclosure;`, (err, response)=>{
            if(err){throw err};

            var hobbiesAdjacent = JSON.parse(fs.readFileSync("./JSON/hobbyAdjacent"));

            hobbiesAdjacent.forEach((hobby)=>{
                console.log(hobby);

                var query = `INSERT INTO hobby(HobbyID, Name, Type, Version, ParentID, Modification) VALUES (${hobby.HobbyID}, "${hobby.Name}", "${hobby.Type}", ${hobby.Version}, ${hobby.ParentID ? hobby.ParentID : "NULL"}, "${hobby.Update}")`;
                console.log(query);
                db.query(query, (err, result)=>{
                    if(err){throw err};
                    var ancestor = hobby.HobbyID;
                    var newDescendant = hobby.ParentID;
                    if(hobby.ParentID == undefined){
                        newDescendant = ancestor;
                    }
                    var query2 = `INSERT INTO hobbyclosure (Ancestor, Descendant, Depth) SELECT Ancestor, ${ancestor}, (Depth + 1) FROM hobbyclosure WHERE descendant = ${newDescendant} UNION ALL SELECT ${hobby.HobbyID}, ${hobby.HobbyID}, 0`;
                    db.query(query2, (err, result)=>{
                        if(err){console.log("error2"); throw err};
                    });
                });
            })
        })
    })
}

closureTableRecursive = function(db, categories, descendant, depth){
    console.log("closureTableRecursive");

    if(categories == null){return}
    categories.forEach((hobby)=>{
        db.beginTransaction((err)=>{
            if(err){db.rollback(()=>{throw err})};

            var query = `INSERT INTO hobby(HobbyID, Name, Type, Version) VALUES (${hobby.HobbyID}, "${hobby.name}", "${hobby.type}", ${hobby.version})`;
            db.query(query, (err, result)=>{
                if(err){db.rollback(()=>{throw err})};
                var ancestor = hobby.HobbyID;
                var newDescendant = descendant;
                if(descendant == undefined){
                    newDescendant = ancestor;
                }
                var query2 = `INSERT INTO hobbyclosure (Ancestor, Descendant, Depth) SELECT Ancestor, ${ancestor}, (Depth + 1) FROM hobbyclosure WHERE descendant = ${newDescendant} UNION ALL SELECT ${hobby.HobbyID}, ${hobby.HobbyID}, 0`;
                db.query(query2, (err, result)=>{
                    db.commit(err, ()=>{
                        if(err){db.rollback(()=>{throw err})};
                        closureTableRecursive(db, hobby.categories, ancestor);
                    })
                });
            })
        })
    })
}


createAllHobby = function (db) {
    console.log("createAllActivities");
    db.query(`TRUNCATE TABLE hobby`, (err, response)=>{
        if(err){throw err};
        recursiveHobby(db, null, categories, 1);
    })
}

recursiveHobby = function(db, parent, categories, rank){
    if(categories.length == 0){
        return
    }

    categories.forEach((hobby)=>{

        db.query(`SELECT HobbyID FROM hobby WHERE Name = "${parent}"`, (err, result)=>{
            if(err){console.log(err, result); throw err};

            if(result.length){
                db.query(`INSERT INTO Hobby SET Name = "${hobby.name}", ParentID = "${result[0].HobbyID}", type = "${hobby.type}", rank=${rank}`, (err, response)=>{
                    if(err){throw err};
                    db.commit((err)=> {
                        if (err) {db.rollback(function() {throw err;});}
                        recursiveHobby(db, hobby.name, hobby.categories, rank + 1);
                    })
                })
            }else{
                db.query(`INSERT INTO Hobby SET Name = "${hobby.name}", type = "${hobby.type}", rank=${rank}`, (err, response)=>{
                    if(err){throw err};
                    db.commit((err)=> {
                        if (err) {db.rollback(function() {throw err;});}
                        recursiveHobby(db, hobby.name, hobby.categories, rank + 1);
                    })
                })
            }
        });
    })
}

getUserHobbyGET = function(db, req, res){
    db.beginTransaction(`
    SELECT distinct c1.HobbyID, c1.name, c1.ParentID, c1.Type FROM hobby c1
        LEFT JOIN hobby c2 ON c2.ParentID = c1.HobbyID
        RIGHT JOIN appuserhobby  ON appuserhobby.HobbyID = c1.HobbyID
     WHERE AppUserID=${req.tokenData.AppUserID}`, (err, result)=>{
        if(err){db.rollback(()=>{throw err})};
        utils.makeJSONResponse(res, "found hobby", null, null, {categories : result});
    })
}

getHobbyDescendantsByDepth = function(db, req, res){
    console.log("getHobbyDescendantsByDepth");
    console.log(req.query);

    db.beginTransaction(`SELECT c.* FROM hobby c
    JOIN hobbyclosure cc ON (c.HobbyID = cc.Descendant)
    WHERE cc.ancestor = ${req.query.HobbyID} AND depth = ${req.query.Depth ? req.query.Depth : 1};`, (err, result)=>{
        if(err){db.rollback(()=>{throw err})};
        utils.makeJSONResponse(res, "worked", null, null, {hobbies : result});
    })
}

removeHobbyFromUser = (db, req, res)=> {
    console.log("removeHobbyFromUser");
    db.query(`DELETE FROM appuserhobby WHERE AppUserID=${req.tokenData.AppUserID} AND HobbyID=${req.body.HobbyID}`, (err, result)=>{
        if(err){throw err}
        utils.makeJSONResponse(res, "removed", null, null);
    })
}

findHobbyByTokenGET = function(db, req, res){

    var query = req.query;
    console.log(`findHobbyByTokenGET: ${query.token}`);

    db.query(`SELECT * FROM Hobby WHERE name LIKE "%${req.query.token}%" AND Type="child"`, (err, result)=>{
        if(err){throw err}
        if(result.length){
            utils.makeJSONResponse(res, "found", null, null, {array : JSON.parse(JSON.stringify(result))});
        }else{
            utils.makeJSONResponse(res, "not found", null, null);
        }
    })
}


//HobbyID
addHobbyToUserPOST = function(db, req, res) {
    console.log("addHobbyToUserPOST");

    db.query(`SELECT * FROM hobby WHERE HobbyID=${req.body.HobbyID}`, (err, result) => {
        if (err) {console.log(err);throw err}
        if (result.length) {
            db.query(`SELECT * FROM appuserhobby WHERE AppUserID=${req.tokenData.AppUserID} AND HobbyID=${req.body.HobbyID}`, (err, result) => {
                if (err) {console.log(result);throw result};
                if (result.length) {
                    res.json({message: "alreadyExists"});
                } else {
                    db.query(`INSERT INTO appuserhobby SET AppUserID=${req.tokenData.AppUserID}, HobbyID=${req.body.HobbyID}`, (err, result) => {
                        if (err) {
                            console.log(err);
                            throw err
                        }
                        utils.makeJSONResponse(res, "hobbyAdded");
                    })
                }
            })
        }else{
            res.json({message: "hobbyDoNotExists"});
        }
    })
}

getHobbiesByVersion = (db, Version, callback)=>{

    console.log("getHobbiesByVersion");
    var query = `SELECT * FROM hobby WHERE Version > ${Version ? Version : 0}`;
    db.query(query, (err, result)=>{
        if(err){throw err};
        if(result.length){
            callback(result);
        }else{
            callback("empty");
        }
    })
}

getAllDatabase = function(db, callback) {
    console.log("addHobbyToUserPOST");

    db.query(`SELECT * FROM hobby`, (err, hobby) => {
        db.query(`SELECT * FROM hobbyclosure`, (err, hobbyclosure) => {

            callback(hobby, hobbyclosure);
        })
    })
}


getAppUserHobby = (db, AppUserID, callback)=>{

    db.query(`SELECT HobbyID FROM appuserhobby WHERE AppUserID=${AppUserID}`, (err, result)=> {
        if(err){throw err}
        if(result.length){
            callback(result);
        }else{
            callback("empty")
        }
    })
}

module.exports = {
                    getAppUserHobby,
                    createClosureFromAdjacent,
                    createAllHobby,
                    removeHobbyFromUser,
                    getHobbyDescendantsByDepth,
                    findHobbyByTokenGET,
                    addHobbyToUserPOST,
                    getUserHobbyGET,
                    createClosureTable,
                    getHobbiesByVersion
};