var util = require("./utils");
var moment = require("moment");
var notification = require("./notification");
var request = require("request")
var firebase = require("./firebase");

getAllChats = function(db, AppUserID, callback){
    console.log("getAllChats");
    const query = `SELECT * FROM chat WHERE (Owner=${AppUserID} OR Receiver = ${AppUserID})`
    console.log(query)

    db.query(query, (err, result)=>{
        if(err){throw err}
        if(result.length){
            callback(result);
        }else{
            callback("empty");
        }
    })
}

getChatBeforeAfter = function(db, chatTimestamp, AppUserID, callback){
    console.log("getChatBeforeAfter");

    db.query(
        `SELECT * FROM chat WHERE Datetime >= '${chatTimestamp}' AND (Owner=${AppUserID} OR Receiver = ${AppUserID})
        UNION ALL
        (SELECT * FROM chat WHERE Datetime <= '${chatTimestamp}' AND (Owner=${AppUserID} OR Receiver = ${AppUserID}) ORDER BY Datetime DESC LIMIT 15)
        ORDER BY datetime ASC;`, (err, result)=>{
            if(err){throw err}

            if(result.length){
                callback(result);
            }else{
                callback("noChanges");
            }
        }
    )
}

getChatUpdates = function(db, AppUserID, ChatUpdatedTS, callback){
    console.log("getChatUpdates");

    const query = `SELECT * FROM chat 
                    WHERE CreatedTS > '${ChatUpdatedTS}' AND (Owner=${AppUserID} OR Receiver = ${AppUserID})
                    ORDER BY CreatedTS ASC`

    db.query( query, (err, result)=>{
        if(err){throw err}
        if(result.length){
            callback(result);
        }else{
            callback("noChanges")
        }
    })

}

getNewMessagesFromUserGET = function(db, req, res){
    console.log("getNewMessagesFromUser");

    var body = req.query
    var tokenData = req.tokenData

    console.log(body)
    console.log(tokenData)


    var query = `SELECT * FROM chat 
                WHERE ChatID > ${body.lastestChatID} AND ((Owner=${body.AppUserID} AND Receiver = ${tokenData.AppUserID}) OR (Owner=${tokenData.AppUserID} AND Receiver =${body.AppUserID})) 
                ORDER BY CreatedTS ASC;`

    console.log(query)

    db.query(
        query, (err, result)=>{
            if(err){throw err}
            if(result.length){


                util.makeJSONResponse(res, "foundNewMessages", null, null, {newMessagesFound : result})

            }else{
                util.makeJSONResponse(res, "notFoundNewMessages")
            }
        }
    )
}

readNewMessages = function (db, req, res) {

    var ids = JSON.parse(req.body.ChatIDS);
    console.log(ids.join(", "));
    console.log(`UPDATE chat SET State=4 WHERE ChatID IN (${ids.join(", ")})`);

    db.query(`UPDATE chat SET State=4 WHERE ChatID IN (${ids.join(", ")})`, (err, result)=>{
        db.query(`SELECT ChatID FROM chat WHERE ChatID IN (${ids.join(", ")});`, (err, response)=>{
            if(err){throw err};
            var updatedIds = response.map((chat)=>{
                return chat.ChatID;
            })
            util.makeJSONResponse(res, "chat message updated", null, null, {ChatIDs : updatedIds})
        })
    })
}

receivedMessageFromFirebasePOST = function(db, req, res){

    var body = req.body;
    var tokenData = req.tokenData;
    db.query(`UPDATE chat SET state=3 WHERE ChatID=${body.ChatID} AND Receiver=${tokenData.AppUserID}`, (err)=>{
        if(err){throw err}
        res.json({message : "received"});
    });
}

//Receiver, Message, SentDatetime, UUID

sendMessagePOST = function(db, AppUserID, message, callback){
    console.log("sendMessagePOST")

    console.log(message)



    var query = `INSERT INTO chat(UUID, Owner, Receiver, Message, SentDatetime) VALUES(
                        '${message.UUID}', 
                        '${AppUserID}', 
                        '${message.Receiver}', 
                        '${message.Message}', 
                        '${message.SentDatetime}' 
            )`

    var queryCheckIfFriend = `Select * From Friend WHERE AppUserID=${AppUserID} AND AppUserFriendID=${message.Receiver} AND STATE=2`


    console.log(queryCheckIfFriend)

    db.query(queryCheckIfFriend, (err, response)=>{
        if(err){throw err};

        if(response.length) {
            db.query(query, (err, response)=>{
                if(err){throw err};
                db.query(`SELECT * FROM chat WHERE ChatID=${response.insertId}`, (err, response)=> {
                    if(err){throw err};
                    callback(JSON.parse(JSON.stringify(response[0])))
                })
            })
        }else {
            callback(null)
        }
    })


}

recordMessage = function(db, req, res){

}

//states
    //0 - send
    //1 - server received
    //2 - read

module.exports = {getAllChats, getChatBeforeAfter, sendMessagePOST, readNewMessages, receivedMessageFromFirebasePOST, getNewMessagesFromUserGET, getChatUpdates};