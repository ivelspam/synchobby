var utils = require("./utils");

//EventID

getEventMessagesGET = (db, req, res)=>{
    console.log("getEventMessagesGET");
    var query = req.query;

    console.log("getEventMessagesGET");
    db.query(`SELECT * FROM eventcommentinfo WHERE EventID=${query.EventID} ORDER BY EventCommentID`, (err, result)=>{
        if(err){throw err};

        if(result.length){
            utils.makeJSONResponse(res, "foundMessage", null, null, {comments : result});
        }else{
            utils.makeJSONResponse(res, "notFoundMessage", null, null, {comments : result});
        }
   })
}

sendACommentPOST = (db, req, res)=>{
    console.log("getSendAMessagePOST");
    var body = req.body;
    var tokenData = req.tokenData;


    let query = `INSERT INTO eventcomment (EventID, Comment, AppUserID) VALUES (${body.EventID}, '${body.Comment}', ${tokenData.AppUserID})`
    console.log(query)
    db.query(query, (err, result)=>{
        if(err){throw err};
        db.query(`SELECT * FROM eventcommentinfo WHERE EventCommentID=${result.insertId}`, (err, result)=>{
            if(err){throw err};
            utils.makeJSONResponse(res, "sended", null, null, {eventcomment : result[0]});
        })
    })
}

sendACommentSocketIOPOST = (db, AppUserID, EventComment, callback)=>{
    console.log("sendACommentSocketIOPOST");

    console.log(AppUserID)
    console.log(EventComment)
    let query = `INSERT INTO eventcomment (EventID, Comment, AppUserID) VALUES (${EventComment.EventID}, '${EventComment.Comment}', ${EventComment.AppUserID})`
    console.log(query)
    db.query(query, (err, result)=>{
        if(err){throw err};
        db.query(`SELECT * FROM eventcommentinfo WHERE EventCommentID=${result.insertId}`, (err, result)=>{
            if(err){throw err};

            console.log(JSON.parse(JSON.stringify(result[0])))
            callback(JSON.parse(JSON.stringify(result[0])));
        })
    })
}


module.exports = {getEventMessagesGET, sendACommentPOST, sendACommentSocketIOPOST};
