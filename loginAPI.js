var bcryptjs = require("bcryptjs");
var utils = require("./utils");

var emailLoginPOST = function(db, req, res){
    console.log("emailLoginPOST");

    var body = req.body;
    console.log(body);

    if(req.body.Email === undefined || req.body.Password === undefined){
        utils.makeJSONResponse(res, "missing parameters");
    }else {


        console.log()
        db.query(`SELECT * FROM appuseremailuser WHERE Email="${req.body.Email}" AND AccountStatus!=2`, (err, result) => {
            if (err) throw console.log(err);

            if (result.length) {
                bcryptjs.compare(req.body.Password, result[0].Password, (err, response) => {
                    if (response) {

                        utils.makeJSONResponse(res, "match", {
                            Email: result[0].Email,
                            AppUserID: result[0].AppUserID,
                            AccountStatus: result[0].AccountStatus
                        }, "Keys/confirmed.key");
                    } else {
                        utils.makeJSONResponse(res, "noMatch");
                    }
                })
            } else {
                utils.makeJSONResponse(res, "noMatch");
            }
        })
    }
}

module.exports = {emailLoginPOST}