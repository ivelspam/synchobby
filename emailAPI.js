var bcryptjs = require("bcryptjs");
var jwt = require("jsonwebtoken");
var fs = require("fs-extra");
var Handlebars = require("handlebars");
var api_key = '';
var DOMAIN = 'synchobby.com';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: DOMAIN});
var mw = require("./middleware");
var utils = require("./utils");

var addUserPOST = function(db, req, res){
    console.log("addUserPOST");

    var passHash = bcryptjs.hashSync(req.body.Password, bcryptjs.genSaltSync(10));
    var tokenExpirationTime = Math.floor(Date.now() / 1000) + (2 * 60 * 60);
    var body = req.body;

    db.query(`SELECT * FROM AppUserEmailUser WHERE Email="${req.body.Email}" AND AccountStatus!=2`, (err, result)=>{
        if(err){throw err;}

        if(result.length){
            var AppUserEmailUser = result[0];
            if(AppUserEmailUser.Password){
                utils.makeJSONResponse(res, "alreadyExists");
            }else {
                db.query(`SELECT * FROM facebookIdUser WHERE Email="${body.Email}"`, (err, result) => {
                    syncWithFacebook(db, AppUserEmailUser.AppUserID, passHash, (AppUser)=>{
                        var emailPayload = {AppUserID : AppUser.AppUserID, type : "new", exp : tokenExpirationTime};
                        var jwtToken = jwt.sign(emailPayload, fs.readFileSync("Keys/login.key", "utf-8"));
                        authenticationEmail(jwtToken, AppUser.Name, AppUser.Email);




                        var payload = {AppUserID : AppUser.AppUserID, confirmed : 0, login: "email"};
                        utils.makeJSONResponse(res, "registered", payload, "Keys/notConfirmed.key", {appuseremailuser : AppUser});
                    });
                })
            }
        }else{
            addANewUser(db, body, passHash,  (AppUser)=>{

                var emailPayload = {AppUserID : AppUser.AppUserID, type : "new", exp : tokenExpirationTime};
                var jwtToken = jwt.sign(emailPayload, fs.readFileSync("Keys/login.key", "utf-8"));
                authenticationEmail(jwtToken, AppUser.Name, AppUser.Email);


                var payload = {AppUserID : AppUser.AppUserID, AccountStatus : 0, login: "email"};
                utils.makeJSONResponse(res, "registered", payload, "Keys/notConfirmed.key", {info : AppUser}, {appuseremailuser : result[0]});
            });
        }
    });
}





var addANewUser = function(db, body, passHash, callback){
    console.log("addANewUser");
    db.beginTransaction((err)=>{
        if(err) {db.rollback(()=>{throw err})};

        db.query(`SELECT * FROM appuser WHERE Email="${body.Email}"`, (err, foundUser)=>{
            if(err){db.rollback(()=>{throw err})};

            console.log(foundUser)

            if(foundUser.length){

                const query = `REPLACE INTO AppUser SET AppUserID=${foundUser[0].AppUserID}, Name="${body.Name}", Email="${body.Email}", Username="${body.Name}${foundUser[0].AppUserID}"`
                console.log(query)
                db.query(query, (err, response)=>{
                    if(err){db.rollback(()=>{throw err})};

                    console.log(response);
                    var AppUserID = response.insertId;
                    db.query(`REPLACE INTO EmailUser SET AppUserID="${foundUser[0].AppUserID}", Password="${passHash}", Confirmed=${0}`, (err, response)=> {
                        if(err){db.rollback(()=>{throw err})}
                        db.query(`SELECT AppUserID, Name, Email, Username From appuser WHERE AppUserID="${AppUserID}"`, (err, response)=> {
                            if(err) {throw err};
                            db.commit((err)=>{
                                if(err){db.rollback(()=>{throw err})};
                                callback(response[0]);
                            })
                        })
                    })
                })

            }else{
                db.query(`SELECT AUTO_INCREMENT FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'social' AND TABLE_NAME='appuser'`, (err, response)=>{
                    if(err){db.rollback(()=>{throw err})};
                    db.query(`INSERT INTO AppUser SET Name="${body.Name}", Email="${body.Email}", Username="${body.Name}${response[0].AUTO_INCREMENT}"`, (err, response)=>{
                        if(err){db.rollback(()=>{throw err})};
                        var AppUserID = response.insertId;
                        db.query(`INSERT INTO EmailUser SET AppUserID="${AppUserID}", Password="${passHash}", Confirmed=${0}`, (err, response)=> {
                            if(err){db.rollback(()=>{throw err})}
                            db.query(`SELECT AppUserID, Name, Email, Username From appuser WHERE AppUserID="${AppUserID}"`, (err, response)=> {
                                if(err) {throw err};
                                db.commit((err)=>{
                                    if(err){db.rollback(()=>{throw err})};
                                    callback(response[0]);
                                })
                            })
                        })
                    })
                })
            }
        })
    })
}

var syncWithFacebook = function(db, AppUserID, passHash, callback){
    console.log("syncwithfacebook");

    db.query(`INSERT INTO EmailUser SET AppUserID="${AppUserID}", Password="${passHash}", Confirmed=0`, (err, response)=>{
        if(err){db.rollback(()=>{throw err})};
        db.query(`SELECT AppUserID, Name, Email, Username From appuser WHERE AppUserID="${AppUserID}"`, (err, response)=> {
            if(err) {throw err};
                callback(response[0]);
        })
    })
}


var confirmEmailGET = function(db, req, res){
    console.log("confirmEmailGET")
    console.log(req.query);
    if(req.query.token === undefined)
    {
        utils.makeJSONResponse(res, "missing parameters");
    }else{
        jwt.verify(req.query.token, fs.readFileSync("Keys/login.key", "utf8"), function(err, data){
            if(err){
                res.sendStatus(403);
            } else {
                console.log(data);
                if(data.type != "new"){
                    res.sendStatus(403);
                }else{
                    db.query(`SELECT EmailOneTimeTokenID FROM emailonetimetoken WHERE token="${req.query.token}"`, (err, result)=>{
                        console.log(err);

                        if(err) {throw err;}
                        if(result.length){
                            res.json("Token Found");
                        }else{
                            db.beginTransaction((err)=>{
                                if(err){db.rollback(()=>{throw err})};
                                db.query(`UPDATE EmailUser SET Confirmed=1 WHERE AppUserID=${data.AppUserID}`, (err, result)=>{
                                    if (err) {db.rollback(function() {throw err;});}
                                    db.query(`UPDATE AppUser SET AccountStatus=1 WHERE AppUserID=${data.AppUserID}`, (err, result)=>{
                                        if (err) {db.rollback(function() {throw err;});}
                                        db.query(`INSERT INTO EmailOneTimeToken SET Token="${req.query.token}", ExpirationTime=${data.exp}`, (err, result)=>{
                                            if (err) {db.rollback(function() {throw err;});}
                                            db.commit((err)=>{
                                                if(err)db.rollback(()=>{throw err;})
                                                res.render("Email Confirmed");
                                            })
                                        });
                                    });
                                });
                            })
                        }
                    });
                }
            }
        });
    }
}

var emailWantToSyncPost = function(db, req, res){
    if( req.body.Name === undefined || req.body.Email === undefined || req.body.Password === undefined){
        res.json("Missing Parameters");
    }else{
        var hash = bcryptjs.hashSync(req.body.Password, bcryptjs.genSaltSync(10));
        var tokenExpirationTime = Math.floor(Date.now() / 1000) + (60 * 60);
        var emailExpirationTime = Math.floor(Date.now() / 1000) + (2 * 24 * 60 * 60);

        db.query(`SELECT * FROM AppUserEmailUser WHERE Email="${req.body.Email}"`, (err, result)=>{
            if(err){ throw err}

            if(result.length){
                var object = result[0];
                if(object.Password){
                    utils.makeJSONResponse(res, "already exists");
                }else{
                    db.beginTransaction((err)=>{
                        if(err) {db.rollback(()=>{ throw err})};

                        db.query(`INSERT INTO EmailUser SET AppUserID="${object.AppUserID}", Password="${hash}", Confirmed=${0}`, (err, response)=> {
                            if(err){db.rollback(()=>{throw err})}

                            db.commit((err)=>{
                                if(err){db.rollback(()=>{throw err})};

                                authenticationEmail(jwt.sign({AppUserID : object.AppUserID, type : "new", exp : tokenExpirationTime}, fs.readFileSync("Keys/login.key", "utf-8")));
                                res.json({message : "synced", token : jwt.sign({AppUserID : object.AppUserID, confirmed : 0}, fs.readFileSync("Keys/confirmed.key", "utf-8"))});
                            })
                        })
                    })
                }
            }else{
                utils.makeJSONResponse(res, "nothing to sync");
            }
        });
    }
}

var sync = function(db, req, res){

    var body = req.body;

    if( req.body.Name === undefined || req.body.Email === undefined || req.body.Password === undefined){
        utils.makeJSONResponse(res, "missing parameters");
    }else{
        var hash = bcryptjs.hashSync(req.body.Password, bcryptjs.genSaltSync(10));
        var expirationTime = Math.floor(Date.now() / 1000) + (60 * 60);
        db.query(`SELECT * FROM AppUserEmailUser WHERE Email="${req.body.Email}"`, (err, result)=>{
            if(err){ throw err}
            if(result.length){
                var object = result[0];
                if(object.Password){
                    utils.makeJSONResponse(res, "exists");
                }else{
                    utils.makeJSONResponse(res, "sync");

                    // db.query(`INSERT INTO TemporaryEmailSignIn SET AppUserID=${object.AppUserID}, Password=${hash}, ExpirationTime=${expirationTime}`, (err, result)=>{
                    //     res.json({message : "registered", token : jwt.sign({AppUserID : AppUserID, confirmed : 0}, fs.readFileSync("Keys/confirmed.key", "utf-8"))});
                    // });
                }
            }else{
                db.beginTransaction((err)=>{
                    if(err) {db.rollback(()=>{ throw err})};
                    db.query(`INSERT INTO AppUser SET Name="${req.body.Name}", Email="${req.body.Email}"`, (err, response)=>{
                        if(err){db.rollback(()=>{throw err})};
                        var AppUserID = response.insertId;
                        db.query(`INSERT INTO EmailUser SET AppUserID="${AppUserID}", Password="${hash}", Confirmed=${0}`, (err, response)=> {
                            if(err){db.rollback((err)=>{throw err})}
                            db.commit((err)=>{             if(err){db.rollback(()=>{throw err})};


                                var jwtToken = jwt.sign({AppUserID : AppUserID, type : "sync", exp : expirationTime}, fs.readFileSync("Keys/login.key", "utf-8"));
                                authenticationEmail(jwtToken, body.Name, body.Email);
                                utils.makeJSONResponse(res, "registered", {AppUserID : AppUserID, confirmed : 0}, "Keys/confirmed.key");
                            })
                        })
                    })
                })
            }
        });
    }
}

authenticationEmail = (token, name, email)=>{

    console.log("authenticationEmail");
    var template = fs.readFileSync("./templates/emailConfirmation/emailConfirmation.hbs", "utf-8");
    var compiledTemplate = Handlebars.compile(template);
    var result2 = compiledTemplate({name : name, token : token})

    console.log(name, email);

    var data = {
        from: 'SyncHobby <ivelacc@synchobby.com>',
        to: email,
        subject: 'SyncHobby Confirmation',
        html: result2
    };

    mailgun.messages().send(data, function (error, body) {
        if(error){console.log(error)}
    });
}


sendAnotherToken = (db, req, res)=>{
    console.log("sendAnotherToken");

    var AppUser = req.AppUser;

    if(!AppUser.AccountStatus){
        var tokenExpirationTime = Math.floor(Date.now() / 1000) + (2 * 60 * 60);

        var emailPayload = {AppUserID : AppUser.AppUserID, type : "new", exp : tokenExpirationTime};

        var jwtToken = jwt.sign(emailPayload, fs.readFileSync("Keys/login.key", "utf-8"));

        authenticationEmail(jwtToken, AppUser.Name, AppUser.Email);
        utils.makeJSONResponse(res, "sent");
    }else{
        utils.makeJSONResponse(res, "registeredAlready");
    }
}

isEmailConfirmed = (db, req, res)=>{
    console.log("isEmailConfirmed");

    const tokenData = req.tokenData;

    db.query(`SELECT * FROM appuseremailuser WHERE AppUserID=${tokenData.AppUserID} AND AccountStatus=1`, (err, result)=>{
        if(err){ throw err}
        if(result.length){
            utils.makeJSONResponse(res, "confirmed", {AppUserID : result[0].AppUserID, AccountStatus : result[0].AccountStatus}, "Keys/confirmed.key");
        }else{
            utils.makeJSONResponse(res, "notConfirmedYet");
        }
    })
}

module.exports = {addUserPOST, confirmEmailGET, emailWantToSyncPost, sendAnotherToken, isEmailConfirmed}



// var addUserPOST = function(db, req, res){
//     console.log("/registration/email/adduser");
//     if( req.body.Name === undefined || req.body.Email === undefined || req.body.Password === undefined){
//         utils.makeJSONResponse(res, "missing parameters");
//     }else{
//         var hash = bcryptjs.hashSync(req.body.Password, bcryptjs.genSaltSync(10));
//         var tokenExpirationTime = Math.floor(Date.now() / 1000) + (2 * 60 * 60);
//         var emailExpirationTime = Math.floor(Date.now() / 1000) + (2 * 24 * 60 * 60);
//
//         db.query(`SELECT * FROM AppUserEmailUser WHERE Email="${req.body.Email}"`, (err, result)=>{
//             if(err){throw err;}
//
//             if(result.length){
//                 var object = result[0];
//                 if(object.Password){
//                     utils.makeJSONResponse(res, "already exists");
//                 }else{
//                     utils.makeJSONResponse(res, "do you want to sync?");
//                 }
//             }else{
//                 db.beginTransaction((err)=>{
//                     if(err) {db.rollback(()=>{throw err})};
//                     db.query(`SELECT AUTO_INCREMENT FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'social' AND TABLE_NAME   = 'appuser'`, (err, response)=>{
//                         if(err){db.rollback(()=>{throw err})};
//                         db.query(`INSERT INTO AppUser SET Name="${req.body.Name}", Email="${req.body.Email}", Username="${req.body.Name}${response[0].AUTO_INCREMENT}"`, (err, response)=>{
//                             if(err){db.rollback(()=>{throw err})};
//                             var AppUserID = response.insertId;
//                             db.query(`INSERT INTO EmailUser SET AppUserID="${AppUserID}", Password="${hash}", Confirmed=${0}`, (err, response)=> {
//                                 if(err){db.rollback(()=>{throw err})}
//                                 db.query(`SELECT AppUserID, Name, Email, Username From appuser WHERE AppUserID="${AppUserID}"`, (err, response)=> {
//                                     if(err) {throw err};
//                                     db.commit((err)=>{
//                                         if(err) {throw err};
//                                         authenticationEmail(jwt.sign({AppUserID : AppUserID, type : "new", exp : tokenExpirationTime}, fs.readFileSync("Keys/login.key", "utf-8")));
//                                         res.json({message : "registered", info: response[0], token : jwt.sign({AppUserID : AppUserID, confirmed : 0, login: "email",}, fs.readFileSync("Keys/confirmed.key", "utf-8"))});
//                                     })
//                                 })
//                             })
//                         })
//                     })
//                 })
//             }
//         });
//     }
// }



