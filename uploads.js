var bcryptjs = require("bcryptjs");
var jwt = require("jsonwebtoken");
var fs = require("fs-extra");
var Handlebars = require("handlebars");
var api_key = 'key-d5b74580d0e458d1355891ff8fcb61ab';
var DOMAIN = 'sandboxfb42c6e155274aa689ff8a3853c878b1.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: DOMAIN});
var mw = require("./middleware");
var glob = require("glob");
var multer = require("multer");
var sharp = require("sharp");
var mimetypes = require("mime-types");
var utils = require("./utils");
var profilePicture = require("./profilePictures");

//multer
var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, 'public/tempProfilePictures/')
    },
    filename: function (req, file, callback) {
        var tokenData = req.tokenData;
        console.log(tokenData)

        profilePicture.deleteOldPicture(tokenData.AppUserID, ()=>{
            callback(null, `${tokenData.AppUserID}-new`);
        })
    }
})

var upload = multer({ storage: storage }).single("profileImage");

uploadProfileImagePOST = function(db, req, res){
    console.log("uploadProfileImagePOST");
    var tokenData = req.tokenData;

    upload(req, res, function (err) {
            if (err) {
                utils.makeJSONResponse(res, "err");
                return
            }

            profilePicture.editAndSavePicture(db, req.AppUser, function(fileInfo){
                utils.makeJSONResponse(res, "done");
            })
    })
}



module.exports = {uploadProfileImagePOST};