var utils = require("./utils");
var LINKS = require("./TableValues").LINKS;


registerEventPost = (db, req, res)=>{
    console.log("registerEventPost")

    var body = req.body,
        tokenData = req.tokenData,
        AppUser = req.AppUser;

    // console.log(body);

    if(AppUser.EventID){
        utils.makeJSONResponse(res, "userAlreadyInEvent");
        return;
    }

    console.log(body);

    db.beginTransaction((err)=>{
        if(err){throw err};

        let query = `INSERT INTO event SET
                        Address="${body.Address}",
                        AppUserID=${tokenData.AppUserID},
                        Description="${body.Description}",
                        EndDatetime="${body.EndDatetime}",
                        HobbyID=${body.HobbyID},
                        Latitude=${body.Latitude},
                        Longitude=${body.Longitude},
                        Name="${body.Name}",
                        PlaceName="${body.PlaceName}",
                        Reach="${body.Reach}",
                        StartDatetime="${body.StartDatetime}"
                    `


        db.query(query, (err, result)=>{
            if(err){db.rollback(()=>{throw err})};

            db.query(`SELECT * FROM eventfullinfo WHERE AppUserID=${AppUser.AppUserID} AND State=0`, (err, result)=>{
                if(err){db.rollback(()=>{throw err})};

                var event = result[0];
                    db.query(`UPDATE appuser SET EventID=${result[0].EventID} WHERE AppUserID=${AppUser.AppUserID}`, ()=>{
                        if(err){throw err};

                        db.commit(err, ()=>{
                            if(err){db.rollback(()=>{throw err})};

                            utils.makeJSONResponse(res, "eventCreated", null, null, {event : event});
                        })
                    })
                })
        });
    })
}


eventsByDistanceWithUserAppIDGET = function (db, req, res) {
    console.log("eventsByDistanceWithUserAppIDGET");
    db.query(`SELECT Distance FROM appuser WHERE AppUserID=${db.tokenData.AppUserID}`, (err, result)=>{
        if(err){throw err};

        if(result.length){
            eventsByDistanceWithUserAppID(db, req.tokenData.AppUserID, result[0].Distance, Latitude, Longitude, function(result){
                if(result.length){
                    utils.makeJSONResponse(res, "found", null, null, {events : eventsRunning})
                }else{
                    utils.makeJSONResponse(res, "found")
                }
            })
        }else{
            utils.makeJSONResponse(res, "user not found");
        }
    })
}

eventsByDistanceWithUserAppID = function (db, AppUserID, MeasureSystem, Distance,  Latitude, Longitude, callback) {
    console.log("eventsByDistanceWithUserAppID");
    console.log(AppUserID, MeasureSystem, Distance,  Latitude, Longitude);



    var radius = 3959

    if(MeasureSystem == "km"){
        radius = 6366
    }

    if(!Latitude){
        callback("empty")
        return
    }

    var query = `SELECT EventFullInfo.*, ( ${radius} * acos(cos(radians(${Latitude})) * cos(radians(latitude))
            * cos( radians( longitude ) - radians(${Longitude})) + sin( radians(${Latitude}) ) * sin(radians(latitude)) ) ) AS distance
            FROM EventFullInfo
            WHERE State < 2
            HAVING distance < ${Distance}
            ORDER BY distance`;

    console.log(query)

    db.query(query, (err, result)=> {
        if(err){throw err};
        console.log(result);

        if(result.length){
            callback(result);
        }else{
            callback("empty")
        }
    })
}

getUserInEventInfo = function (db, EventID, callback) {
    console.log("getUserInEventInfo");

    if(EventID == null){
        callback("empty");
        return
    }

    var query = `SELECT EventFullInfo WHERE EventID=${EventID}`;
    console.log(query);
    db.query(query, (err, result)=> {
        if(err){throw err};
        if(result.length){
            callback(result[0])
        }else{
            callback("empty");
        }
    })
}

getUserInEventInfoUpdated = function (db, EventID, EventUpdatedTS, callback) {
    console.log("getUserInEventInfoUpdated");

    if(EventID == null){
        callback("noChanges");
        return
    }

    var query = `SELECT * FROM EventFullInfo WHERE EventID=${EventID} AND UpdatedTS>'${EventUpdatedTS}'`;

    db.query(query, (err, result)=> {
        if(err){throw err};
        if(result.length) {
            callback(result[0]);
        }else{
            callback("noChanges");
        }

    })
}

eventsByDistanceWithUserAppIDOthers = function (db, AppUserID, Distance, MeasureSystem, eventsIDs, latitude, longitude, callback) {
    console.log("eventsByDistanceWithUserAppIDOthers");
    console.log(AppUserID, Distance, MeasureSystem, eventsIDs, latitude, longitude,);

    var radius = 3959

    if(MeasureSystem == "km"){
        radius = 6366
    }

    console.log(eventsIDs);

    var query = `SELECT EventFullInfo.*, ( ${radius} * acos(cos(radians(${latitude})) * cos(radians(latitude))
        * cos( radians( longitude ) - radians(${longitude})) + sin( radians(${latitude}) ) * sin(radians(latitude)) ) ) AS distance
        FROM EventFullInfo
        WHERE State < 2 ${eventsIDs ? `AND EventID NOT IN ${eventsIDs}` : ``}
        HAVING Distance < ${Distance}
        ORDER BY Distance`;

    console.log(query);
    db.query(query, (err, result)=> {
        if(err){throw err};

        if(result.length){
            callback(result);
        }else{
            callback("empty")
        }
    })
}

eventsByDistanceFilteredWithHobbyAndReach = function (db, AppUserID, Distance, MeasureSystem, latitude, longitude, callback) {
    console.log("eventsByDistanceFilteredWithHobbyAndReach");
    console.log(AppUserID, Distance, MeasureSystem, latitude, longitude);

    var radius = 3959

    if(MeasureSystem == "km"){
        radius = 6366
    }

    console.log(eventsIDs);
    var query = `SELECT *, ( ${radius} * acos( cos( radians(  ${latitude}) ) * cos( radians( latitude ) )
        * cos( radians( longitude ) - radians(${longitude}) ) + sin( radians(${latitude}) ) * sin(radians(latitude)) ) ) AS distance
    FROM eventfullinfo
    LEFT JOIN appuserhobby ON eventfullinfo.HobbyID = appuserhobby.HobbyID
    WHERE appuserhobby.AppUserID = ${AppUserID}
    State < 2
    AND
    (
		REACH = 0
        OR ( REACH = 1 AND eventfullinfo.AppUserID IN (SELECT AppUserFriendID FROM friend WHERE AppUserID = ${AppUserID}))
		OR ( REACH = 2 AND eventfullinfo.AppUserID IN (SELECT DISTINCT AppUserID FROM friend WHERE AppUserFriendID IN ( SELECT AppUserFriendID FROM friend WHERE AppUserID = ${AppUserID})  AND AppUserID != ${AppUserID}))
    );      
    HAVING Distance < ${Distance}`;


    console.log(query);

    db.query(query, (err, result)=> {
        if(err){throw err};

        if(result.length){
            callback(result);
        }else{
            callback("empty")
        }
    })
}

eventsNoInEventsIDsByDistanceFilteredWithHobbyAndReach = function (db, AppUserID, Distance, MeasureSystem, eventsIDs, hobbiesIDs, latitude, longitude, callback) {
    console.log("eventsNoInEventsIDsByDistanceFilteredWithHobbyAndReach");
    console.log(AppUserID, Distance, MeasureSystem, eventsIDs, hobbiesIDs, latitude, longitude);


    if(hobbiesIDs != ""){
        var radius = 3959

        if(MeasureSystem == "km"){
            radius = 6366
        }

        var query = `SELECT EventFullInfo.*, ( ${radius} * acos( cos( radians(  ${latitude}) ) * cos( radians( latitude ) )
        * cos( radians( longitude ) - radians(${longitude}) ) + sin( radians(${latitude}) ) * sin(radians(latitude)) ) ) AS distance
    FROM eventfullinfo
    LEFT JOIN appuserhobby ON eventfullinfo.HobbyID = appuserhobby.HobbyID
    WHERE appuserhobby.AppUserID = ${AppUserID}
    AND State < 2
    ${hobbiesIDs ? `AND HobbyID IN ${hobbiesIDs}` : ""} 
    ${eventsIDs ? `AND EventID NOT IN ${eventsIDs}` : ""}
    AND
    (
        REACH = 0
        OR ( REACH = 1 AND eventfullinfo.AppUserID IN (SELECT AppUserFriendID FROM friend WHERE AppUserID = ${AppUserID} ))
        OR ( REACH = 2 AND eventfullinfo.AppUserID IN (SELECT DISTINCT AppUserID FROM friend WHERE AppUserFriendID IN ( SELECT AppUserFriendID FROM friend WHERE AppUserID = ${AppUserID} )))
    )
    HAVING Distance < ${Distance}`;

        console.log(query);

        db.query(query, (err, result)=> {
            if(err){throw err};

            if(result.length){
                callback(result);
            }else{
                callback("noChanges")
            }
        })
    }else{
        callback("noChanges")
    }
}

eventsUpdate = function (db, req, res) {

    var query = `SELECT EventID, State FROM event WHERE HobbyID IN ${body.hobbiesIDs}`
    db.query(query, (err, result)=> {
        if(err){throw err};

        res.json(utils.makeJSONResponse(res, message, null, null, {events : result}));
    })
}

deleteEventPOST = function (db, req, res) {
    console.log("deleteEventPOST");
    var AppUser = req.AppUser;
    var body = req.body;

    db.beginTransaction((err)=>{

        db.query(`SELECT * FROM event WHERE AppUserID=${AppUser.AppUserID}`, (err, result)=>{
            if(err){db.rollback(()=>{throw err})};


            if(result.length){

                var response = LINKS.event_delete.Response.closedDeletedEvent;

                if(result[0].state == 1){ response = LINKS.event_delete.Response.deletedEvent }

                db.query(`UPDATE event SET state=2, QuantityOfPeople=0 WHERE AppUserID=${AppUser.AppUserID}`, (err, result)=>{
                    if(err){db.rollback(()=>{throw err})};
                    db.query(`UPDATE appuser SET EventID = NULL WHERE EventID=${body.EventID}`, (err, result)=>{
                        if(err){db.rollback(()=>{throw err})};
                        db.query(`SELECT * FROM EventFullInfo WHERE EventID=${body.EventID}`, (err, result)=> {
                            if(err){db.rollback(()=>{throw err})};
                            db.commit((err) => {
                                if(err){db.rollback(()=>{throw err})};
                                utils.makeJSONResponse(res, response, null, null, {event : result[0]});
                            })
                        })
                    })
                })

            }else{
                utils.makeJSONResponse(res, "eventNotFound");
            }
        })
    })
}

leaveEventPOST = function (db, req, res) {
    console.log("LeaveEventPOST");

    var AppUser = req.AppUser;
    var body = req.body;

    console.log(body);

    db.beginTransaction((err)=>{
        if(err){db.rollback(()=>{throw err})};

        db.query(`UPDATE appuser SET EventID=NULL WHERE AppUserID=${AppUser.AppUserID}`, (err, response)=>{
            if(err){db.rollback(()=>{throw err})};
            db.query(`UPDATE event SET QuantityOfPeople=QuantityOfPeople-1 WHERE EventID=${body.EventID}`, (err, result)=>{
                if(err){db.rollback(()=>{throw err})};
                db.query(`SELECT * FROM EventFullInfo WHERE EventID=${body.EventID}`, (err, result)=> {
                    if(err){db.rollback(()=>{throw err})};
                    db.commit((err) => {
                        if(err){db.rollback(()=>{throw err})};
                        utils.makeJSONResponse(res, "leftEvent", null, null, {event: result[0]});
                    })
                })
            })
        })
    })
}

joinEventPOST = function (db, req, res) {
    console.log("joinEventPOST");

    var tokenData = req.tokenData;
    var body = req.body;

    console.log(body)


    if(tokenData.EventID == null){

        db.beginTransaction((err)=>{
            if(err){db.rollback(()=>{throw err})};

            db.query(`UPDATE appuser SET EventID=${body.EventID} WHERE AppUserID=${tokenData.AppUserID}`, (err, result)=>{
                if(err){db.rollback(()=>{throw err})};
                db.query(`UPDATE event SET QuantityOfPeople=QuantityOfPeople+1 WHERE EventID=${body.EventID}`, (err, result)=>{
                    if(err){db.rollback(()=>{throw err})};
                    db.query(`SELECT * FROM EventFullInfo WHERE EventID=${body.EventID}`, (err, events)=> {
                        db.commit((err)=>{
                            if(err){db.rollback(()=>{throw err})};
                            utils.makeJSONResponse(res, "joinedEvent", null, null, {event: events[0]});
                        })
                    })
                })
            })
        })
    }else{
        utils.makeJSONResponse(res, "alreadyInEvent");


    }


}

closeEventPOST = function (db, req, res) {
    console.log("closeEventPOST");
    var AppUser = req.AppUser;
    var body = req.body;

    console.log(AppUser);
    console.log(req.body);

    db.beginTransaction((err)=>{
        if(err){db.rollback(()=>{throw err})};

        db.query(`UPDATE event SET state=1, QuantityOfPeople=0 WHERE AppUserID=${AppUser.AppUserID}`, (err, result)=>{
            if(err){db.rollback(()=>{throw err})};
            db.query(`UPDATE appuser SET EventID = NULL WHERE EventID=${body.EventID}`, (err, result)=>{
                if(err){db.rollback(()=>{throw err})};
                db.query(`SELECT * FROM EventFullInfo WHERE EventID=${body.EventID}`, (err, result)=> {
                    if(err){db.rollback(()=>{throw err})};
                    db.commit((err) => {
                        if(err){db.rollback(()=>{throw err})};
                        utils.makeJSONResponse(res, "closedEvent", null, null, {event : result[0]});
                    })
                })
            })
        })
    })
}

hideEventPOST = function (db, req, res) {
    console.log("hideEventPOST");
    var appUser = req.AppUser;

    db.beginTransaction((err)=> {
        if(err){db.rollback(()=>{throw err})};

        db.query(`UPDATE event SET hidden=1 WHERE EventID=${appUser.EventID} AND AppUserID=${appUser.AppUserID}`, (err, result) => {
            if(err){db.rollback(()=>{throw err})};
            db.query(`SELECT * FROM EventFullInfo WHERE EventID=${req.body.EventID}`, (err, result) => {
                if(err){db.rollback(()=>{throw err})};
                db.commit((err) => {
                    if(err){db.rollback(()=>{throw err})};
                    utils.makeJSONResponse(res, "hidedEvent", null, null, {event: result[0]});
                })
            })
        })
    })
}

showEventPOST = function (db, req, res) {
    console.log("hideEventPOST");
    var appUser = req.AppUser;
    db.beginTransaction((err)=> {
        if(err){db.rollback(()=>{throw err})};

        db.query(`UPDATE event SET Hidden=0 WHERE EventID=${appUser.EventID} AND AppUserID=${appUser.AppUserID}`, (err, result)=>{
            if(err){db.rollback(()=>{throw err})};
            db.query(`SELECT EventFullInfo WHERE EventID =${req.body.EventID} }`, (err, result)=> {
                if(err){db.rollback(()=>{throw err})};
                db.query(`SELECT EventFullInfo WHERE EventID =${req.body.EventID} }`, (err, result)=> {
                    if(err){db.rollback(()=>{throw err})};
                    db.commit((err) => {
                        if(err){db.rollback(()=>{throw err})};
                        utils.makeJSONResponse(res, "showEvent", null, null, {event : result[0]});

                    })
                })
            })
        })
    })
}

getEventsChanges = function (db, eventsIDs, EventUpdatedTS, callback) {
    console.log("getEventsChanges");

    if(eventsIDs != ""){
        var query = `SELECT EventID, Hidden, QuantityOfPeople, State, UpdatedTS FROM event WHERE EventID IN ${eventsIDs} AND UpdatedTS>'${EventUpdatedTS}'`;
        db.query(query, (err, result)=>{
            if(err){throw  err}
            if (result.length) {
                callback(result);
            }else{
                callback("noChanges")
            }
        })
    }else{
        callback("noChanges")
    }
}


findAndEndEvent = function (db, eventIDs, UpdatedTS, callback) {
    console.log("findAndEndEvent");
    if(eventIDs != ""){
        var query = `SELECT EventID, QuantityOfPeople, State FROM event WHERE EventID IN ${eventIDs} AND UpdatedTS>'${UpdatedTS}'`;

        db.query(query, (err, result)=>{
            if(err){throw  err}

            if (result.length) {
                callback(result);
            }else{
                callback("noChanges")
            }
        })
    }else{
        callback("noChanges")
    }

}

module.exports = {
                    leaveEventPOST,
                    registerEventPost,
                    eventsByDistanceWithUserAppIDGET,
                    eventsByDistanceWithUserAppID,
                    joinEventPOST,
                    hideEventPOST,
                    closeEventPOST,
                    eventsNoInEventsIDsByDistanceFilteredWithHobbyAndReach,
                    eventsByDistanceFilteredWithHobbyAndReach,
                    getEventsChanges,
                    getUserInEventInfo,
                    eventsByDistanceWithUserAppIDOthers,
                    getUserInEventInfoUpdated,
                    showEventPOST,
                    deleteEventPOST
                };

/*
    Event
    STATES:
        0 : Created
        1 : Ended
        2 : Deleted
 */

/*
    Event
    Reach:
        0 : Everyone
        1 : Friends
        2 : Friends and Friends Of Friends

*/