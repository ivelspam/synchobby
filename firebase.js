var request = require("request");
var utils = require('./utils')

var sendNotification = function(db, AppUserIDs, payload, type, callback) {
    db.query(`SELECT fcmtoken.FCMTokenID FROM fcmtoken WHERE AppUserID IN (${AppUserIDs.join()})`, (err, result)=>{
        if(err){throw err};

        if(result.length){
            console.log(result);
            var deviceIds = result.map((result)=>{
                return result.FCMTokenID
            })
            request({
                url: 'https://fcm.googleapis.com/fcm/send',
                method: 'POST',
                headers: {
                    'Content-Type': ' application/json',
                    'Authorization': 'key=AAAAX5Yf7HY:APA91bGPYU7ZvUphVDBuukWxnbKLD8rF70MsKWO7Im3clP5_G5faYWSqCcNwxsQRNrIAb0DIM1ZVKqTDVgkNszpsv79MOBUzQxuegczNrlIb3ECPCP-Yhgz9DN7wyk72sjjcPKwAk4CB'
                },
                body: JSON.stringify(
                    {
                        data : {
                            messageJSON : payload,
                            type : type
                        },
                        notification: {
                            notification : "notification",
                            body: "this is the body"
                        },
                        registration_ids : deviceIds
                    }
                )
            }, function (error, response, body) {
                if (error) {
                    console.error(error, response, body);
                }
                else if (response.statusCode >= 400) {
                    console.error('HTTP Error: ' + response.statusCode + ' - ' + response.statusMessage + '\n' + body);
                } else{
                    if(callback){
                        callback(body);
                    }
                }
            });
        }else{
            callback(`{"success" : false}`)
        }
    })
}

var getFirebaseCustomToken = (db, firebaseAdmin, req, res)=> {
    console.log("getFirebaseCustomToken")

    console.log(req.AppUser)
    console.log(req.AppUser.AppUserID)
    getCustomToken(firebaseAdmin, req.AppUser.AppUserID, (customToken) => {

        if(customToken){
            utils.makeJSONResponse(res, "worked", null, null, {customToken: customToken})
        }else{
            utils.makeJSONResponse(res, "false", null, null, {customToken: customToken})
        }

    })

}


var getCustomToken = (firebaseAdmin, uid, callback)=>{

    console.log(uid)

    firebaseAdmin.auth().createCustomToken(`${uid}`)
    .then((customToken)=>{
        console.log(customToken)
        callback(customToken)
    })
    .catch((error) => {
        console.log('Error creating custom token:', error)
        callback(false)
    })

}


module.exports = {sendNotification, getFirebaseCustomToken};
