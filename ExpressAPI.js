//express
const hbs = require("express-handlebars");
const mw = require("./middleware");
const subdomain = require("express-subdomain")

//server
const express = require('express')
const https = require("https");
const http = require("http");

const bodyParser = require('body-parser')
const socketio = require("socket.io")
const vhost = require('vhost')
const firebaseAdmin = require('firebase-admin')
const tls = require('tls')

//database
const mysql = require("mysql")

//others
const cron = require("./cron")
const fs = require("fs-extra")
var jwt = require("jsonwebtoken");


//APIS
const tokensAPI = require("./tokensAPI");
const facebookAPI = require("./facebookAPI");
const emailAPI = require("./emailAPI");
const infoAPI = require("./infoAPI");
const downloads = require("./downloads")
const uploads = require("./uploads");
const friends = require("./friends");
const categoriesAPI = require("./Hobby");
const loginAPI = require("./loginAPI");
const event = require("./event");
const utils = require("./utils");
const chat = require("./chat");
const appuser = require("./appuser");
const fcmtoken = require("./fcmtoken");
const myFirebaseAPI = require('./firebase')
const EventComments = require("./EventComment");

//constants
const LINKS = require('./TableValues').LINKS;
const Constants = require("./TableValues")


require(`./Prototypes`);


//Routers

const db = mysql.createConnection({
    host : "localhost",
    user : "root",
    password : "root",
    database : "social",
    timezone : "utc"
})

db.connect((err)=>{
    if(err){throw err}
    console.log("MySql Connected... ");
})

const router = express.Router({});
const router2 = express.Router({});

router2.get('/', function(req, res) {
    const file = './letsencryptVerification';

    console.log(file)

    res.download(file);
});

router.get('/', function(req, res) {
    console.log("GET /");
    utils.makeJSONResponse(res, "hooray! welcome to our api! GET");
});
router.post('/', function(req, res) {
    console.log("POST /");
    utils.makeJSONResponse(res, "hooray! welcome to our api! GET");
});


//privacy
router.get('/privacy', function(req, res){
    res.render("privacy");
});

router.get('/privacy', function(req, res){
    res.render("privacy");
});

router.post('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api! POST' });
});

// categoriesAPI.createClosureTable(db);
// categoriesAPI.createClosureFromAdjacent(db);

//test everything
router.get('/testEverything', function(req, res) { console.log("testEverything") });

//event
router.post(`/${LINKS.event_register.name}`,  mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function(req, res){ event.registerEventPost(db, req, res) });
router.post(`/${LINKS.event_leave.name}`,  mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function(req, res){ event.leaveEventPOST(db, req, res) });
router.post(`/${LINKS.event_close.name}`,  mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function(req, res){ event.closeEventPOST(db, req, res) });
router.post(`/${LINKS.event_join.name}`,  mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function(req, res){ event.joinEventPOST(db, req, res) });
router.post(`/${LINKS.event_hide.name}`,  mw.ensureToken, mw.authTokenPrivate,  mw.checkIfUserExists(db), function(req, res){ event.hideEventPOST(db, req, res) });
router.post(`/${LINKS.event_show.name}`,  mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function(req, res){ event.showEventPOST(db, req, res) });
router.post(`/${LINKS.event_delete.name}`,  mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function(req, res){ event.deleteEventPOST(db, req, res) });


//eventcomment
router.get(`/${LINKS.eventcomment_getEventComments.name}`, function(req, res){ EventComments.getEventMessagesGET(db, req, res)})

//AppUser
router.post(`/${LINKS.appuser_deleteAccount.name}`, mw.ensureToken, mw.authTokenPrivate, function(req, res) {appuser.deleteAccount(db, req, res)});
router.post(`/${LINKS.appuser_updateUsername.name}`, mw.ensureToken, mw.authTokenPrivate, function(req, res) {appuser.updateUsername(db, req, res)});
router.post(`/${LINKS.appuser_updateName.name}`, mw.ensureToken, mw.authTokenPrivate, function(req, res) {appuser.updateName(db, req, res)});
router.get( `/${LINKS.appuser_findAUserByUsername.name}`,function(req, res) {appuser.findAUserByUsernameGET(db, req, res);});
router.post(`/${LINKS.appuser_UpdateDistanceAndMeasurementSystem.name}`, mw.ensureToken, mw.authTokenPrivate, function(req, res) {appuser.updateDistanceAndMeasurement(db, req, res);});
router.get(`/${LINKS.appuser_getImageVersion.name}`, function(req, res) {appuser.getUserImageVersion(db, req, res);});
router.get( `/${LINKS.appuser_findAUserByUsername.name}`,function(req, res) {appuser.findAUserByUsernameGET(db, req, res);});
router.get( `/${LINKS.appuser_checkIfExists.name}`, mw.ensureToken, mw.authTokenNotConfirmed, function(req, res) {appuser.checkIfUserExists(db, req, res);});

//hobby
router.post(`/${LINKS.hobbies_removeHobby.name}`,   mw.ensureToken, mw.authTokenPrivate, function(req, res) {categoriesAPI.removeHobbyFromUser(db, req, res);});
router.post(`/${LINKS.hobbies_addHobby.name}`,  mw.ensureToken, mw.authTokenPrivate, function(req, res) {categoriesAPI.addHobbyToUserPOST(db, req, res);});

//chat
router.get(`/${LINKS.chat_getNewFromFriend.name}`, mw.ensureToken, mw.authTokenPrivate, function (req, res) { chat.getNewMessagesFromUserGET(db, req, res) });

//Downloads
router.get(`/${LINKS.download_image.name}`, function(req, res){
    downloads.downloadImageGET(db, req, res)
});

//upload
router.post(`/${LINKS.upload_profilePicture.name}`, mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function (req, res) {uploads.uploadProfileImagePOST (db, req, res)});

//email login
router.post(`/${LINKS.login_email.name}`, function(req, res) { loginAPI.emailLoginPOST(db, req, res)});
router.post(`/${LINKS.login_facebook.name}`, function(req, res) { facebookAPI.facebookLoginPOST(db, req, res);});

//email registration
router.post(`/${LINKS.email_addUser.name}`, function(req, res) { emailAPI.addUserPOST(db, req, res); });
router.get(`/${LINKS.email_confirmation.name}`, function(req, res) { emailAPI.confirmEmailGET(db, req, res); });
router.post(`/${LINKS.email_sendAnotherToken.name}`,  mw.ensureToken, mw.authTokenNotConfirmed, mw.checkIfUserExists(db), function(req, res) { emailAPI.sendAnotherToken(db, req, res); });
router.get(`/${LINKS.email_isConfirmed.name}`,  mw.ensureToken, mw.authTokenNotConfirmed, mw.checkIfUserExists(db), function(req, res) { emailAPI.isEmailConfirmed(db, req, res); });

//facebook login
router.post('/facebook/login', function(req, res) { facebookAPI.facebookLoginPOST(db, req, res) });

//fcmtoken
router.post(`/${LINKS.fcmToken_removeTokenFromSystem.name}`, mw.ensureToken, mw.authTokenPrivate, function (req, res) { fcmtoken.removeFCMPOST(db, req, res) })

//friends
router.post(`/${LINKS.friend_sendRequest.name}`, mw.ensureToken, mw.authTokenPrivate, function(req, res) { friends.sendRequest(db, req, res); });
router.post(`/${LINKS.friend_accept.name}`, mw.ensureToken, mw.authTokenPrivate, function(req, res) { friends.acceptRequestPOST(db, req, res); });
router.post(`/${LINKS.friend_cancel.name}`, mw.ensureToken, mw.authTokenPrivate,function(req, res) { friends.cancelRequestPOST(db, req, res); });
router.post(`/${LINKS.friend_delete.name}`,  mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db),function(req, res){ friends.deleteFriend(db, req, res) });


//Info
router.post(`/${LINKS.allInfo_getAllInfo.name}`, mw.ensureToken, mw.authTokenPrivate, function(req, res) {infoAPI.getAllInfoPost(db, req, res);});
router.post(`/${LINKS.allInfo_getUpdates.name}`, mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function(req, res) {infoAPI.getAllInfoUpdatePost(db, req, res);});

//test
router.post('/test',  mw.ensureToken, mw.authTokenPrivate, function(req, res){ res.sendStatus(403) });

//token
router.get(`${LINKS.token_getNewToken.name}`, mw.ensureToken, mw.authTokenPrivate, function(req, res) { tokensAPI.getNewTokenGET(db, req, res) });

//firebase
router.get('/firebase/getCustomToken', mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function(req, res) { myFirebaseAPI.getFirebaseCustomToken(db, firebaseAdmin, req, res) });

router.get("/checkToken", mw.ensureToken, function(req, response){})

// router.post('/', function(req, res) {
//     res.json({ message: 'hooray! welcome to our api! POST' });
// });

// var server = app.listen(80, function(){
//     console.log('listening for requests on port 4000,');
// });


const mainExpress = express()
mainExpress.use(bodyParser.urlencoded({ extended: true }));
mainExpress.use(bodyParser.json());
mainExpress.use(express.static("public"));
mainExpress.engine("hbs", hbs({extname : "hbs", layoutsDir: __dirname + "/templates"}))
    .set("view engine", "hbs");
mainExpress.use('/api', router);

// const mainExpress2 = express()
mainExpress.use(subdomain('_acme-challenge', router2));


// mainExpress.use("_acme-challenge", router2)

const privateKey  = fs.readFileSync('ssl2/privkey.pem', 'utf8');
const certificate = fs.readFileSync('ssl2/fullchain.pem', 'utf8');


const credentials = {key: privateKey, cert: certificate};

var httpServer = http.createServer(mainExpress).listen(80);
const httpsServer = https.createServer(credentials, mainExpress).listen(443);

//sockets
const io = socketio.listen(httpsServer);

//{appUserID : 1, socketID : asdfvxcvasdf}

const usersConnected = {}


io.use((socket, next)=>{

    console.log("Trying to authenticating socket token")
    console.log(socket.handshake)

    if (socket.handshake.query && socket.handshake.query.token){
        jwt.verify(socket.handshake.query.token,  fs.readFileSync("Keys/confirmed.key", "utf8"), function(err, decoded) {
            if(err) return next(new Error('Authentication error'));
            socket.decoded = decoded;
            next();
        });
    } else {
        next(new Error('Authentication error'));
    }
}).on('connection', function (socket) {
    console.log("New Connection")

    var query = socket.handshake.query;

    const values = { AppUserID : socket.decoded.AppUserID, socket : socket, socketid : socket.id}

    if(usersConnected[values.AppUserID] != undefined){
        usersConnected[values.AppUserID].socket.disconnect
    }

    usersConnected[values.AppUserID] = values

    socket.on("disconnect", (data)=>{
        console.log("disconnect")
        delete usersConnected[query.AppUserID]
    })

    socket.on(Constants.SocketIOConnections.sendChatMessage, function (data, fn) {
        const message = JSON.parse(data)

        chat.sendMessagePOST(db, values.AppUserID, message, (response)=>{
            fn(response)
            if(response != null){
                socket.to(Constants.SocketIOConnections.friendChat(response.Owner, response.Receiver)).emit(response)
            }
        })
    });

    socket.on(Constants.SocketIOConnections.serverReceiveEventComment, function (data, fn) {
        let EventComment = JSON.parse(data);
        EventComments.sendACommentSocketIOPOST(db, query.AppUserID, EventComment, (response)=>{
            fn(response)
            socket.broadcast.emit(Constants.SocketIOConnections.clientReceiveEventMessage(EventComment.EventID), response)
        })
    });
});

cron.updateEvents(db);

// db.query("SELECT * FROM eventcomment ", (err, result)=>{
//     console.log(result)
// })


//firebase

// firebaseAdmin.initializeApp({
//     credential: firebaseAdmin.credential.cert(serviceAccount)
// })
// const uid = "some-uid"
//
// firebaseAdmin.auth().createCustomToken(uid)
//     .then((customToken)=>{
//         console.log(customToken)
//     })
//     .catch((error) => {
//         console.log('Error creating custom token:', error)
//     })


//event
// router.post('/event/registerevent',  mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function(req, res){ event.registerEventPost(db, req, res) });
// router.post('/event/leaveEvent',  mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function(req, res){ event.leaveEventPOST(db, req, res) });
// router.post('/event/closeEvent',  mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function(req, res){ event.closeEventPOST(db, req, res) });
// router.post('/event/joinEvent',  mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db),function(req, res){ event.joinEventPOST(db, req, res) });
// router.post('/event/hideEvent',  mw.ensureToken, mw.authTokenPrivate,  mw.checkIfUserExists(db), mw.checkIfUserExists(db), function(req, res){ event.hideEventPOST(db, req, res) });
// router.post('/event/eventsByDistanceFiltered',  mw.ensureToken, mw.authTokenPrivate,  mw.checkIfUserExists(db),function(req, res){ event.eventsByDistanceFiltered(db, req, res) });


// var query = `SELECT * FROM friendinfo WHERE ViewAppUserID=${1}`;
//
// console.log(query)
// db.query(query, (err, result) => {
//     if (err) { console.log(err);throw err; }
//     console.log(result)
// })