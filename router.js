//
// router.get('/', function(req, res) {
//     console.log("GET /");
//     utils.makeJSONResponse(res, "hooray! welcome to our api! GET");
// });
// router.post('/', function(req, res) {
//     console.log("POST /");
//     utils.makeJSONResponse(res, "hooray! welcome to our api! GET");
// });
//
//
// //privacy
// router.get('/privacy', function(req, res){
//     res.render("privacy");
// });
//
// router.post('/', function(req, res) {
//     res.json({ message: 'hooray! welcome to our api! POST' });
// });
//
// // categoriesAPI.createClosureTable(db);
// // categoriesAPI.createClosureFromAdjacent(db);
//
// //test everything
// router.get('/testEverything', function(req, res) { console.log("testEverything") });
//
// //event
// router.post('/event/registerevent',  mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function(req, res){ event.registerEventPost(db, req, res) });
// router.post('/event/leaveEvent',  mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function(req, res){ event.leaveEventPOST(db, req, res) });
// router.post('/event/closeEvent',  mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function(req, res){ event.closeEventPOST(db, req, res) });
// router.post('/event/joinEvent',  mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db),function(req, res){ event.joinEventPOST(db, req, res) });
// router.post('/event/hideEvent',  mw.ensureToken, mw.authTokenPrivate,  mw.checkIfUserExists(db), mw.checkIfUserExists(db), function(req, res){ event.hideEventPOST(db, req, res) });
// router.post('/event/eventsByDistanceFiltered',  mw.ensureToken, mw.authTokenPrivate,  mw.checkIfUserExists(db),function(req, res){ event.eventsByDistanceFiltered(db, req, res) });
//
// //eventcomment
// router.get("/eventcomment/getEventComments", function(req, res){ EventComments.getEventMessagesGET(db, req, res)})
// router.post("/eventcomment/sendAMessage",  mw.ensureToken, mw.authTokenPrivate, function(req, res){ EventComments.getSendAMessagePOST(db, req, res)})
//
// //AppUser
// router.post('/appuser/updateusername', mw.ensureToken, mw.authTokenPrivate, function(req, res) {appuser.updateUsername(db, req, res)});
// router.post('/appuser/updatename', mw.ensureToken, mw.authTokenPrivate, function(req, res) {appuser.updateName(db, req, res)});
// router.get( '/appuser/findAUserByUsername',function(req, res) {appuser.findAUserByUsernameGET(db, req, res);});
// router.post('/appuser/updateMeasureSystem', mw.ensureToken, mw.authTokenPrivate, function(req, res) {appuser.updateMeasureSystemPOST(db, req, res);});
// router.post('/appuser/updateUserDistance', mw.ensureToken, mw.authTokenPrivate, function(req, res) {appuser.updateUserDistancePOST(db, req, res);});
// router.post('/appuser/updateUserDistanceAndMeasurement', mw.ensureToken, mw.authTokenPrivate, function(req, res) {appuser.updateDistanceAndMeasurement(db, req, res);});
//
// //hobby
// router.get( '/hobbies/findHobbyByToken', function(req, res) {categoriesAPI.findHobbyByTokenGET(db, req, res);});
// router.get( '/hobbies/getUserHobbies',  mw.ensureToken, mw.authTokenPrivate, function(req, res) {categoriesAPI.getUserHobbyGET(db, req, res);});
// router.get( '/hobbies/getHobbyDescendantByDepth',  function(req, res) {categoriesAPI.getHobbyDescendantsByDepth(db, req, res);});
// router.post('/hobbies/removeHobbyFromUser',   mw.ensureToken, mw.authTokenPrivate, function(req, res) {categoriesAPI.removeHobbyFromUser(db, req, res);});
// router.post('/hobbies/addHobbiesToUser',  mw.ensureToken, mw.authTokenPrivate, function(req, res) {categoriesAPI.addHobbyToUserPOST(db, req, res);});
//
// //chat
// router.post("/chat/sendChatMessage", mw.ensureToken, mw.authTokenPrivate, function (req, res) { chat.sendMessagePOST(db, req, res) });
// router.post("/chat/readNewMessages", mw.ensureToken, mw.authTokenPrivate, function (req, res) { chat.readNewMessages(db, req, res) });
// router.post("/chat/receivedMessageFromFirebase", mw.ensureToken, mw.authTokenPrivate, function (req, res) { chat.receivedMessageFromFirebasePOST(db, req, res) });
//
// //Downloads
// router.get('/downloads/pictureVersion', function(req, res){ downloads.downloadPictureVersion(db, req, res) });
// router.get('/downloads/friendProfilePicture',  mw.ensureToken, mw.authTokenPrivate, function(req, res){ downloads.downloadFriendProfilePictureGET(db, req, res) });
// router.get('/downloads/profilePicture',  mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function(req, res){ downloads.downloadProfilePictureGET(db, req, res) });
// router.get('/downloads/downloadAProfilePictureFromPath',  mw.ensureToken, mw.authTokenPrivate, function(req, res){ downloads.downloadAProfilePictureFromPathGET(db, req, res) });
//
// //upload
// router.post('/uploadProfileImage', mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function (req, res) {uploads.uploadProfileImagePOST (db, req, res)});
//
// //email login
// router.post('/login/email', function(req, res) { loginAPI.emailLoginPOST(db, req, res)});
//
// //email registration
// router.post('/registration/email/adduser', function(req, res) { emailAPI.addUserPOST(db, req, res); });
// router.get('/registration/email/confirmEmail', function(req, res) {emailAPI.confirmEmailGET(db, req, res);});
// router.get('/registration/email/getAEmailConfirmedToken', mw.ensureToken, mw.authTokenPrivate, function(req, res) {emailAPI.getAEmailConfirmedToken(db, req, res);});
// router.post('/registration/email/emailWantToSync', function(req, res) { emailAPI.emailWantToSyncPost(db, req, res);});
// router.post('/registration/email/SendAnotherToken',  mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function(req, res) { emailAPI.sendAnotherToken(db, req, res);});
//
// //facebook login
// router.post('/facebook/login', function(req, res) { facebookAPI.facebookLoginPOST(db, req, res) });
// router.post('/facebook/facebookWantToSync', function(req, res) { facebookAPI.facebookWantToSyncPOST(db, req, res);});
//
// //fcmtoken
// router.post("/fcmtoken/removeTokenFromSystem", mw.ensureToken, mw.authTokenPrivate, function (req, res) { fcmtoken.removeFCMPOST(db, req, res) })
//
// //friends
// router.post("/friends/updateReadMessageDatetime", mw.ensureToken, mw.authTokenPrivate, function (req, res) { friends.updateReadMessageDatetime(db, req, res) });
// router.post('/friend/sendRequest', mw.ensureToken, mw.authTokenPrivate, function(req, res)                 { friends.sendRequest(db, req, res); });
// router.post('/friend/acceptRequest', mw.ensureToken, mw.authTokenPrivate, function(req, res)               { friends.acceptRequestPOST(db, req, res); });
// router.post('/friend/cancelRequest', mw.ensureToken, mw.authTokenPrivate,function(req, res)               { friends.cancelRequestPOST(db, req, res); });
//
// //Info
// router.post('/info/getAllInfo', mw.ensureToken, mw.authTokenPrivate, function(req, res) {infoAPI.getAllInfoPost(db, req, res);});
//
// //test
// router.post('/test',  mw.ensureToken, mw.authTokenPrivate, function(req, res){ res.sendStatus(403) });
//
// //token
// router.get('/token/getNewToken', mw.ensureToken, mw.authTokenPrivate, function(req, res) { tokensAPI.getNewTokenGET(db, req, res) });
//
//
// //firebase
// router.get('/firebase/getCustomToken', mw.ensureToken, mw.authTokenPrivate, mw.checkIfUserExists(db), function(req, res) { myFirebaseAPI.getFirebaseCustomToken(db, firebaseAdmin, req, res) });
//
