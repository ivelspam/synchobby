var bcryptjs = require("bcryptjs");
var jwt = require("jsonwebtoken");
var fs = require("fs-extra");
var Handlebars = require("handlebars");
var api_key = 'key-d5b74580d0e458d1355891ff8fcb61ab';
var DOMAIN = 'sandboxfb42c6e155274aa689ff8a3853c878b1.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: DOMAIN});
var mw = require("./middleware");
var glob = require("glob");

//getFiles
downloadFriendProfilePictureGET = function(db, req, res){
    console.log("downloadFriendProfilePictureGET");
    glob(`public/profilePictures/${req.query.AppUserID}-*`, function(er, files){
        console.log(`${__dirname}/${files[0]}`);
        if(files.length){
            res.sendFile(`${__dirname}/${files[0]}`);
        }else{
            res.sendFile(__dirname + "/public/download.jpg");
        }
    })
}

downloadPictureVersion = function(db, req, res){
    console.log("downloadPictureVersion");

    var query = req.query
    console.log(req.query)
    glob(`public/profilePictures/${query.AppUserID}-${query.ImageVersion}*`, function(er, files){
        console.log(`${__dirname}/${files[0]}`);
        if(files.length){
            res.sendFile(`${__dirname}/${files[0]}`);
        }else{
            res.sendFile(__dirname + "/public/download.jpg");
        }
    })
}

downloadProfilePictureGET = function(db, req, res){
    console.log("downloadProfilePictureGET");

    var AppUserID = req.AppUser.AppUserID
    var ImageVersion = req.AppUser.ImageVersion

    let file = `public/profilePictures/${AppUserID}-${ImageVersion}.jpg`

    glob(file, function(er, files){
        if(files.length){
            res.sendFile(`${__dirname}/${files[0]}`);
        }else{
            res.sendFile(__dirname + "/public/download.jpg");
        }
    })
}

downloadImageGET = function(db, req, res){
    console.log("downloadImage");
    const query = req.query;
    console.log(query)
    switch (query.type){
        case "PROFILE":
            getFriendImage(query.AppUserID, query.ImageVersion, res)
            break;
        case "FRIEND":
            getFriendImage(query.AppUserID, query.ImageVersion, res)
            break;
        case "COMMENT":
            getFriendImage(query.AppUserID, query.ImageVersion, res)
            break;
        default:
            throw "Error not valid option"
    }

    // if(req.query.ImagePath){
    //     glob(`public/profilePictures/${req.query.ImagePath}-*`, function(er, files){
    //         if(files.length){
    //             res.sendFile(`${__dirname}/${files[0]}`);
    //         }else{
    //             res.sendFile(__dirname + "/public/download.jpg");
    //         }
    //     })
    // }else{
    //     glob(`public/profilePictures/${req.query.ImageVersion}-*`, function(er, files){
    //         if(files.length){
    //             res.sendFile(`${__dirname}/${files[0]}`);
    //         }else{
    //             res.sendFile(__dirname + "/public/download.jpg");
    //         }
    //     })
    // }
}

getFriendImage = (AppUserID, ImageVersion, res)=>{

    console.log("getFriendImage");
    glob(`public/profilePictures/${AppUserID}-${ImageVersion}.jpg`, function(er, files){
        if(files.length){
            // callback(`${__dirname}/${files[0]}`);
            res.sendFile(`${__dirname}/${files[0]}`)
        }else{
            // callback(`${__dirname}/${files[0]}`);
            res.status(404).send("not found")
        }
    })
}

module.exports = {downloadImageGET};