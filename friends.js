var utils = require("./utils");
var request = require("request")
var firebase = require("./firebase");
var Links = require("./TableValues").LINKS;


var getFriends = function(db, AppUserID, callback) {
    console.log("getFriends2");

    var query = `SELECT * FROM friendinfo WHERE ViewAppUserID=${AppUserID}`;

    console.log(query)
    db.query(query, (err, result) => {
        if (err) { console.log(err);throw err; }
        console.log(result)

        if(result.length){
            callback( result );
        }else{
            callback( "empty" );
        }
    })
}

var getFriendsUpdate = function(db, AppUserID, FriendsUpdatedTS, callback) {
    console.log("getFriendsUpdate");
    var query = `SELECT * FROM friendinfo WHERE ViewAppUserID=${AppUserID} AND UpdatedTS>'${FriendsUpdatedTS}'`

    console.log(query);

    db.query(query, (err, result) => {
        if (err) { console.log(err); throw err; }

        if(result.length){
            console.log(result);

            callback(result);
        }else{
            callback("noChanges");
        }

    })
}


var sendRequest = function(db, req, res){
    console.log("sendRequest");
    var body = req.body;
    var tokenData = req.tokenData;

    db.query(`SELECT * FROM Friend WHERE AppUserID=${tokenData.AppUserID} AND AppUserFriendID=${body.AppUserID} AND State=2`, (err, result)=>{
        if(err){throw err;}

        if(result.length){
            utils.makeJSONResponse(res, "userAlreadyExists");
        }else{
            db.query(`REPLACE INTO Friend (AppUserID, AppUserFriendID, State) VALUES (${tokenData.AppUserID}, ${body.AppUserID}, 0), (${body.AppUserID}, ${tokenData.AppUserID}, 1)`, (err, result)=>{
                var query = `SELECT * FROM FriendInfo WHERE ViewAppUserID=${tokenData.AppUserID} AND AppUserID=${body.AppUserID}`
                db.query(query, (err, result)=>{
                    if(err){throw err};
                    utils.makeJSONResponse(res, "userAdded", null, null, {friendInfo : result[0]});
                })
            });
        }
    });
}

var deleteFriend = function (db, req, res) {
    console.log("deleteFriend");

    var tokenData = req.tokenData;
    var body = req.body;

    const query = `UPDATE Friend SET State=3 WHERE (AppUserID=${tokenData.AppUserID} AND AppUserFriendID=${body.AppUserID}) OR (AppUserID=${body.AppUserID} AND AppUserFriendID=${tokenData.AppUserID})`
    console.log(query)
    db.query(query, (err, result)=>{
        if(err){throw err;}
        utils.makeJSONResponse(res, Links.friend_delete.Response.deleted);
    });
}


var confirmFriend = function(db, req, res){
    console.log("addFriendUserPost");
    db.query(`SELECT * FROM Friend WHERE AppUserID=${req.tokenData.AppUserID} AND AppUserFriendID=${req.body.AppUserID}`, (err, result)=>{
        if(err){throw err;}

        if(result.length){
            utils.makeJSONResponse(res, "user already exists");
        }else{
            utils.makeJSONResponse(res, "user added");
            db.query(`INSERT INTO Friend SET AppUserID = "${req.tokenData.AppUserID}", AppUserFriendID="${req.body.AppUserID}"`, (err, result)=>{
                if(err){throw err;}
                utils.makeJSONResponse(res, "user added", null, null, {user : result[0]});
            });
        }
    });
}

var acceptRequestPOST = function(db, req, res){
    console.log("addFriendUserPost");

    var tokenData = req.tokenData;
    var body = req.body;

    const checkIfValid = `Select * from friend WHERE (AppUserID=${tokenData.AppUserID} AND AppUserFriendID=${body.AppUserID} AND State=1) OR (AppUserID=${body.AppUserID} AND AppUserFriendID=${tokenData.AppUserID} AND State=0)`

    db.query(checkIfValid, (err, result)=>{
        if(err){throw err};

        if(result.length == 2){
            db.query(`UPDATE Friend SET State=2 WHERE (AppUserID=${tokenData.AppUserID} AND AppUserFriendID=${body.AppUserID}) OR (AppUserID=${body.AppUserID} AND AppUserFriendID=${tokenData.AppUserID})`, (err, result)=>{
                if(err){throw err;}
                        utils.makeJSONResponse(res, "accepted");
            })
        }else{
            utils.makeJSONResponse(res, "notPossible");
        }
    })
}


var cancelRequestPOST = function(db, req, res){
    console.log("cancelRequestPOST");

    var tokenData = req.tokenData;
    var body = req.body;

    const query = `DELETE FROM Friend WHERE (AppUserID=${body.AppUserID} AND AppUserFriendID=${tokenData.AppUserID}) OR (AppUserID=${tokenData.AppUserID} AND AppUserFriendID=${body.AppUserID })`;

    db.query(query, (err, result)=>{
        if(err){throw err;}
                if(err){throw err};
                utils.makeJSONResponse(res, "canceled");
    });


}

module.exports = {getFriends, sendRequest, acceptRequestPOST, cancelRequestPOST, getFriendsUpdate, deleteFriend};


/*
* State
* 0 SentRequested
* 1 ReceivedRequest
* 2 Confirmed
* 3 Banned
* 4 Deleted
* 5 AccountClosed

*
* */