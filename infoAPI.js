var bcryptjs = require("bcryptjs");
var jwt = require("jsonwebtoken");
var fs = require("fs-extra");
var Handlebars = require("handlebars");
var api_key = 'key-d5b74580d0e458d1355891ff8fcb61ab';
var DOMAIN = 'sandboxfb42c6e155274aa689ff8a3853c878b1.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: DOMAIN});
var mw = require("./middleware");
var categories = require("./Hobby");
var utils = require("./utils");
var event = require("./event");
var fcmtoken = require("./fcmtoken");
var chat = require("./chat");
var friends = require("./friends");
var appUser = require("./AppUser");
var hobby = require("./hobby");





var getAllInfoPost = function(db, req, res){
    console.log("getAllInfoGet");

    var AppUserID = req.tokenData.AppUserID;
    var body = req.body;
    console.log(body)


     db.beginTransaction((err)=>{
         if(err){db.rollback(()=>{throw err})};
        appUser.getAppUserInfo(db, AppUserID, (appUserInfo)=>{
            if(appUserInfo){
                fcmtoken.insertOrUpdateToken(db, AppUserID, body.FCMToken, body.DeviceID, body.DeviceType, ()=>{
/*friends*/     friends.getFriends(db, AppUserID, (friends)=>{
/*Hobby*/       categories.getAppUserHobby(db, AppUserID, (appUserHobbies)=>{
/*chat*/        chat.getAllChats(db, AppUserID, (chats)=>{
/*events*/      event.eventsByDistanceFilteredWithHobbyAndReach(db, AppUserID,  appUserInfo.Distance, appUserInfo.MeasureSystem, body.Latitude, body.Longitude, (events)=>{
/*in event*/    event.getUserInEventInfo(db, AppUserID.EventID, (inEvent)=>{
/*Hobby*/       hobby.getHobbiesByVersion(db, body.HobbyVersion, (hobbies)=>{
                    db.commit((err)=>{
                        if(err){db.rollback(()=>{throw err})};

                        var response = {};

                        if(appUserHobbies != "empty"){ response.appUserHobbies = appUserHobbies; }
                        if(appUserInfo != "empty"){ response.appUserInfo = appUserInfo; }
                        if(chats != "empty"){ response.chats = chats; }
                        if(events != "empty"){ response.events = events; }
                        if(friends != "empty"){ response.friends = friends; }
                        response.hobbiesTable = hobbies;
                        if(inEvent != "empty"){ response.userInEvent = inEvent; }

                        utils.makeJSONResponse(res, "userFound", null, null, response)
                })
                })
                })
                })
                })
                })
                })
                })
            }else{
                utils.makeJSONResponse(res, "userNotFound");
            }
        })
    })
}


var getAllInfoUpdatePost = function(db, req, res){
    console.log("getAllInfoUpdatePost");

    var AppUser = req.AppUser;
    var body = req.body;
    console.log(body)
    var date = Date();

    db.beginTransaction((err)=> {
        appUser.getAppuserUpdates(db, AppUser.AppUserID, body.UpdatedTS, (AppUserChange)=>{
            friends.getFriendsUpdate(db, AppUser.AppUserID, body.friendUpdatedTS, (friends) => {
                chat.getChatUpdates(db, AppUser.AppUserID, body.lastestChatID, (chats) => {
                    event.getUserInEventInfoUpdated(db, AppUser.EventID, body.inEventUpdatedTS, (inEvent)=>{
                        event.eventsNoInEventsIDsByDistanceFilteredWithHobbyAndReach(  db, AppUser.AppUserID, AppUser.Distance, AppUser.MeasureSystem, body.eventsIDs, body.hobbiesIDs, body.Latitude, body.Longitude, (events) => {
                            event.getEventsChanges(db, body.eventsIDs, body.eventUpdatedTS, (eventsUpdate)=>{
                                db.commit((err) => {
                                    if(err){db.rollback(()=>{throw err})};

                                    var response = {};

                                    if(AppUserChange != "noChanges"){ response.AppUserChange = AppUserChange; }
                                    if(chats != "noChanges"){ response.chats = chats; }
                                    if(events != "noChanges"){ response.events = events; }
                                    if(eventsUpdate != "noChanges"){ response.eventsUpdate = eventsUpdate; }
                                    if(friends != "noChanges"){ response.friends = friends; }
                                    if(inEvent != "noChanges"){ response.inEvent = inEvent; }

                                    console.log(response);

                                    if(Object.keys(response).length === 0 && response.constructor === Object){
                                        utils.makeJSONResponse(res, "noUpdates");
                                    }else{
                                        utils.makeJSONResponse(res, "foundUpdates", null, null, response);
                                    }
                                })
                            })
                        })
                    })
                })
            })
        })
     })
}


module.exports = {getAllInfoPost, getAllInfoUpdatePost};