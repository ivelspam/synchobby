const SocketIOConnections = {
    authentication : "authentication",
    disconnect : "disconnect",
    sendChatMessage : "sendChatMessage",
    serverReceiveEventComment : "serverReceiveEventComment",
    clientReceiveEventMessage : (eventID)=>{
        return `clientReceiveEventMessage${eventID}`
    },

    friendChat : (Owner, Receiver)=>{
        var sb = "friendChat";
        if(Owner < Receiver){
            sb += `${Owner}-${Receiver}`;
        }else{
            sb += `${Receiver}-${Owner}`;
        }
        return sb;
    }

}


const DATABASE = {

    MySQLColumns : {


        AppUserName : "AppUserName",

        Address : "Address",

        AppUserID : "AppUserID",
        Autoincrement : "Autoincrement",

        ChatID : "ChatID",

        DateCreated : "DateCreated",
        Datetime : "Datetime",
        Description : "Description",

        EventID : "EventID",
        EndDatetime : "EndDatetime",

        FriendID : "FriendID",
        HobbyName : "HobbyName",

        HobbyID : "HobbyID",

        ImagePath : "ImagePath",
        ImageVersion : "ImageVersion",
        Latitude : "Latitude",
        Longitude : "Longitude",
        Message : "Message",

        Name : "Name",
        Owner : "Owner",

        ParentID : "ParentID",
        QuantityOfPeople : "QuantityOfPeople",
        Reach : "Reach",

        ReadMessageDatetime : "ReadMessageDatetime",
        Receiver : "Receiver",
        SentDatetime : "SentDatetime",

        State : "State",

        Type : "Type",
        Version : "Version",
        Timestamp : "Timestamp",

        UUID : "UUID",

        StartDatetime : "StartDatetime",
        CreatedTS : "CreatedTS",
        PlaceName : "PlaceName",
        UpdatedTS : "UpdatedTS",
    },

    TABLES : {
        appuser : {},
        appuserhobby : {},
        chat : {},
        emailonetimetoken : {},
        emailuser : {},
        event : {},
        eventcomment : {},
        facebookuser : {},
        fcmtoken : {},
        friend : {},
        hobby : {},
        hobbyclosure : {},
        test : {}
    }
}



const LINKS = {

    //ALLINFO

    allInfo_getAllInfo : {
        name: "allInfo_getAllInfo",
        Response : {
            userFound : "userFound",
            userNotFound : "userNotFound"
        },
        ResponseFields : {
            appUserHobbies : "appUserHobbies",
            appUserInfo : "appUserInfo",
            chats : "chats",
            events : "events",
            friends : "friends",
            hobbiesTable : "hobbiesTable",
            inEvent : "inEvent",
            message : "message"

        },
        Param : {
            DeviceID : DATABASE.MySQLColumns.DeviceID,
            DeviceType : DATABASE.MySQLColumns.DeviceType,
            FCMToken : DATABASE.MySQLColumns.FCMToken,
            Latitude : DATABASE.MySQLColumns.Latitude,
            Longitude : DATABASE.MySQLColumns.Longitude
        }

    },
    allInfo_getUpdates : {
        name: "allInfo_getUpdates",
        Response : {
            foundUpdates : "foundUpdates",
            noUpdates : "noUpdates"
        },
        ResponseFields : {
            AppUserChange : "AppUserChange",
            chats :  "chats",
            inEvent : "inEvent",
            eventsUpdate : "eventsUpdate",
            events : "events",
            friends : "friends",
            message : "message"

        },
        Param : {
            eventsIDs : "eventsIDs",
            eventUpdatedTS : "eventUpdatedTS",
            friendUpdatedTS : "friendUpdatedTS",
            hobbiesIDs : "hobbyIDs",
            inEventsUpdatedTS : "inEventsUpdatedTS",
            latestChatID : "latestChatID",
            Latitude :  DATABASE.MySQLColumns.Latitude,
            Longitude : DATABASE.MySQLColumns.Longitude,
            UpdatedTS : DATABASE.MySQLColumns.UpdatedTS
        }
    },

    //APPUSER
    appuser_deleteAccount : {
        name: "appuser_deleteAccount",
        Response : {
            accountDeleted : "accountDeleted"
        },
        ResponseFields : {},
        Param : {}
    },
    appuser_checkIfExists : {
        name: "appuser_checkIfExists",
        Response : {
            userFound : "userFound",
            userNotFound : "userNotFound"
        },
        ResponseFields : {

        },
        Param : {
        }

    },
    appuser_findAUserByUsername : {
        name: "appuser_findAUserByUsername",
        Response : {
            userFound : "userFound",
            userNotFound : "userNotFound"
        },
        ResponseFields : {
            user : "user",
            message : "message"
        },
        Param : {
            Username : "Username"
        }
    },
    appuser_getImageVersion : {
        name: "appuser_getImageVersion",
        Response : {

        },
        ResponseFields : {

        },
        Param : {

        }
    },

    appuser_UpdateDistanceAndMeasurementSystem : {
        name: "appuser_UpdateDistanceAndMeasurementSystem",
        Response : {
            updated : "updated",
            notUpdated : "notUpdated"
        },
        ResponseFields : {

        },
        Param : {
            MeasureSystem : DATABASE.MySQLColumns.MeasureSystem,
            Distance : DATABASE.MySQLColumns.Distance
        }
    },
    appuser_updateName : {
        name: "appuser_updateName",
        Response : {

        },
        ResponseFields : {

        },
        Param : {
            Name : DATABASE.MySQLColumns.Name
        }
    },
    appuser_updateUsername : {
        name: "appuser_updateUsername",
        Response : {
            foundImage : "foundImage",
            notFoundImage : "notFoundImage"
        },
        ResponseFields : {
            ImageVersion : DATABASE.MySQLColumns.ImageVersion,
            message : "message"
        },
        Param : {
            AppUserID : "AppUserID"
        }
    },

    //CHAT
    chat_getNewFromFriend : {
        name : "chat_getNewFromFriend",
        Response : {
            foundNewMessages : "foundNewMessages",
            notFoundNewMessages : "notFoundNewMessages"
        },
        ResponseFields : {
            newMessagesFound : "newMessagesFound"
        },
        Param : {
            AppUserID : DATABASE.MySQLColumns.AppUserID
        }
    },

    //DOWNLOAD
    download_image : {
        name : "download_image",
        Response : {
            foundMessage : "foundMessage",
            notFoundMessage : "notFoundMessage"

        },
        ResponseFields : {

        },
        Param : {
            AppUserID : DATABASE.MySQLColumns.AppUserID,
            ImageVersion : DATABASE.MySQLColumns.ImageVersion,
            type : "type"
        }
    },

    //EMAIL
    email_addUser : {
        name : "email_addUser",
        Response : {
            registered : "registered",
            alreadyExists : "alreadyExists"
        },
        ResponseFields : {
            token : "token"

        },
        Param : {
            name : DATABASE.MySQLColumns.Name,
            Email : DATABASE.MySQLColumns.Email,
            Password : DATABASE.MySQLColumns.Password,
        }
    },
    email_confirmation : {
        name: "email_confirmation",
        Response : {

        },
        ResponseFields : {

        },
        Param : {

        }
    },
    email_sendAnotherToken : {
        name: "email_sendAnotherToken",
        Response : {

        },
        ResponseFields : {

        },
        Param : {

        }
    },
    email_isConfirmed : {
        name : "email_isConfirmed",
        Response : {

        },
        ResponseFields : {
            token : "token"
        },
        Param : {
            name : DATABASE.MySQLColumns.Name,
            Email : DATABASE.MySQLColumns.Email,
            Password : DATABASE.MySQLColumns.Password,
        }
    },


    //EVENT
    event_close : {
        name: "event_close",
        Response : {
            closedEvent : "closedEvent"
        },
        ResponseFields : {

        },
        Param : {
            EventID : DATABASE.MySQLColumns.EventID
        }
    },

    event_getEventByDistance : {
        name: "event_getEventByDistance",
        Response : {
            closedEvent : "closedEvent"
        },
        ResponseFields : {

        },
        Param : {
            Latitude : DATABASE.MySQLColumns.Latitude,
            Longitude : DATABASE.MySQLColumns.Longitude,
            Distance : DATABASE.MySQLColumns.Distance,
            earthRadius : "earthRadius",
            eventsIDs : "eventsIDs",
            hobbiesIDs : "hobbiesIDs"
        }
    },
    event_delete : {
        name: "event_delete",
        Response : {
            deletedEvent : "deletedEvent",
            closedDeletedEvent : "closedDeletedEvent"

        },
        ResponseFields : {

        },
        Param : {
            EventID : DATABASE.MySQLColumns.EventID
        }
    },
    event_hide : {
        name: "event_hide",
        Response : {
            hidedEvent : "hidedEvent"
        },
        ResponseFields : {

        },
        Param : {
            EventID : DATABASE.MySQLColumns.EventID
        }
    },
    event_show : {
        name: "event_how",
        Response : {
            hidedEvent : "showedEvent"
        },
        ResponseFields : {

        },
        Param : {
            EventID : DATABASE.MySQLColumns.EventID
        }
    },
    event_join : {
        name: "event_join",
        Response : {
            joinedEvent : "joinedEvent",
            alreadyInEvent : "alreadyInEvent"

        },
        ResponseFields : {

        },
        Param : {
            EventID : DATABASE.MySQLColumns.EventID
        }
    },
    event_leave : {
        name: "event_leave",
        Response : {
            joinedEvent : "leftEvent"

        },
        ResponseFields : {

        },
        Param : {
            EventID : DATABASE.MySQLColumns.EventID

        }
    },
    event_register : {
        name: "event_register",
        Response : {
            eventCreated : "eventCreated",
            userAlreadyInEvent : "userAlreadyInEvent"
        },
        ResponseFields : {
            event : "event"
        },
        Param : {
            Address  : DATABASE.MySQLColumns.Address,
            Description : DATABASE.MySQLColumns.Description,
            EndDatetime : DATABASE.MySQLColumns.EndDatetime,
            HobbyID : DATABASE.MySQLColumns.HobbyID,
            Latitude : DATABASE.MySQLColumns.Latitude,
            Longitude : DATABASE.MySQLColumns.Longitude,
            Name : DATABASE.MySQLColumns.Name,
            PlaceName : DATABASE.MySQLColumns.PlaceName,
            Reach : DATABASE.MySQLColumns.Reach,
            StartDatetime : DATABASE.MySQLColumns.StartDatetime
        }
    },


    //EVENTCOMMENT
    eventcomment_getEventComments : {
        name: "eventcomment_getEventComments",
        Response : {
            foundMessage : "foundComments",
            notFoundMessage : "noComments"
        },
        ResponseFields : {
            comments : "comments"
        },
        Param : {
            EventID : DATABASE.MySQLColumns.EventID
        }
    },



    //FCMTOKEN
    fcmToken_removeTokenFromSystem : {
        name : "fcmToken_removeTokenFromSystem",
        Response : {
            removed : "removed"
        },
        ResponseFields : {
            event : "event"
        },
        Param : {

        }
    },

    //FRIEND
    friend_accept : {
        name : "friend_accept",
        Response : {
            accepted : "accepted"
        },
        ResponseFields : {

        },
        Param : {

        }
    },
    friend_cancel : {
        name : "friend_cancel",
        Response : {
            canceled : "canceled"
        },
        ResponseFields : {

        },
        Param : {
            AppUserID : DATABASE.MySQLColumns.AppUserID
        }
    },
    friend_delete : {
        name : "friend_delete",
        Response : {
            deleted : "deleted"
        },
        ResponseFields : {

        },
        Param : {
            AppUserID : DATABASE.MySQLColumns.AppUserID
        }
    },

    friend_sendRequest : {
        name : "friend_sendRequest",
        Response : {
            userAlreadyExists : "userAlreadyExists",
            userAdded : "userAdded"
        },
        ResponseFields : {
            ImageVersion : DATABASE.MySQLColumns.ImageVersion
        },
        Param : {
            AppUserID : "AppUserID"
        }
    },

    //HOBBIES
    hobbies_addHobby : {
        name : "hobbies_addHobby",
        Response : {
            alreadyExists : "alreadyExists",
            hobbyDoNotExists : "hobbyDoNotExists",
            hobbyAdded : "hobbyAdded"
        },
        ResponseFields : {

        },
        Param : {
            HobbyID : "HobbyID"
        }
    },
    hobbies_removeHobby : {
        name : "hobbies_removeHobby",
        Response : {
            removed : "removed"
        },
        ResponseFields : {

        },
        Param : {
            HobbyID : "HobbyID"
        }
    },

    //login
    login_email : {
        name: "login_email",
        Param : {
            Email : "Email",
            Password : "Password"
        },
        Response : {
            noMatch : "noMatch",
            match : "match"
        },
        ResponseFields : {

        },

    },
    login_facebook : {
        name: "login_facebook",
        Response : {
            logged : "logged"
        },
        ResponseFields : {

        },
        Param : {
            FacebookUserID : "FacebookUserID",
            facebookuserToken : "facebookUserToken"
        }
    },

    //TOKEN
    token_getNewToken : {
        name: "token_getNewToken",
        Response : {

        },
        ResponseFields : {

        },
        Param : {

        }
    },

    //UPLOAD

    upload_profilePicture : {
        name : "upload_profilePicture",
        Response : {

        },
        ResponseFields : {

        },
        Param : {

        }
    }



}

module.exports = {DATABASE, LINKS, SocketIOConnections}