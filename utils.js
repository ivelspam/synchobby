var jwt = require("jsonwebtoken");
var fs = require("fs-extra");

makeJSONResponse = function (res, message, payload, key, others) {
    console.log("makeJSONResponse");
    var response = {};
    response.message = message;
    console.log("message: " + message);

    if(payload){
        // console.log(payload);
        response.token = jwt.sign(payload, fs.readFileSync(key, "utf-8"));
    }else{
        console.log("no payload");
    }

    if(others){
        response = Object.assign(response, others);
    }
    // console.log(response);

    res.json(response);
}


module.exports = {makeJSONResponse};

